//
//  EventDetails.swift
//  Ultimate Events
//
//  Created by Greenfield
//  Copyright (c) 2016 Greenfield.com. All rights reserved.
//

import UIKit
import Parse
import EventKit
import MapKit
import MessageUI
import GoogleMobileAds
import AudioToolbox


class EventDetails: UIViewController, MKMapViewDelegate, MFMailComposeViewControllerDelegate, GADBannerViewDelegate
{
    
    @IBOutlet var containerScrollView: UIScrollView!
    @IBOutlet var eventImage: UIImageView!
    @IBOutlet var descrTxt: UITextView!
    @IBOutlet var detailsView: UIView!
    @IBOutlet var addToCalOutlet: UIButton!
    @IBOutlet var shareOutlet: UIButton!
    @IBOutlet var dayNrLabel: UILabel!
    @IBOutlet var weekDayLabel: UILabel!
    @IBOutlet var monthLabel: UILabel!
//    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var registerOutlet: UIButton!
    @IBOutlet var startDateLabel: UILabel!
    @IBOutlet var endDateLabel: UILabel!
    @IBOutlet var costLabel: UILabel!
    @IBOutlet var websiteLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var titleLbl: UILabel!
    
    var backButt = UIButton()
    var reportButt = UIButton()
    
    //AdMob Banner
    var adMobBannerView = GADBannerView()
    
    
    // Vars
    var eventObj = PFObject(className: EVENTS_CLASS_NAME)
    var annotation:MKAnnotation!
    var localSearchRequest:MKLocalSearchRequest!
    var localSearch:MKLocalSearch!
    var localSearchResponse:MKLocalSearchResponse!
    var error:NSError!
    var pointAnnotation:MKPointAnnotation!
    var pinView:MKPinAnnotationView!
    var region: MKCoordinateRegion!
    
    
    func handleSwipeFrom() {
        self.backButton(UIButton())
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(StoreDetails.handleSwipeFrom))
        gesture.direction = [.left , .right]
        
        containerScrollView.addGestureRecognizer(gesture)
        // Back Button
//        backButt = UIButton(type: UIButtonType.custom)
//        backButt.setTitle("Back", for: UIControlState())
//        backButt.setTitleColor(UIColor(red: 246/255, green: 0/255, blue: 82/255, alpha: 1), for: UIControlState())
//        backButt.setTitleColor(UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1), for: UIControlState.highlighted)
//        backButt.titleLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 15)
//        backButt.frame = CGRect(x: 0, y: 0, width: 35, height: 44)
//        backButt.addTarget(self, action: #selector(backButton(_:)), for: UIControlEvents.touchUpInside)
//        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButt)
        
        // Report Button
        reportButt = UIButton(type: UIButtonType.custom)
        reportButt = UIButton(type: UIButtonType.custom)
        reportButt.setTitle("Report", for: UIControlState())
        reportButt.setTitleColor(UIColor(red: 246/255, green: 0/255, blue: 82/255, alpha: 1), for: UIControlState())
        reportButt.setTitleColor(UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1), for: UIControlState.highlighted)
        reportButt.titleLabel?.font = UIFont(name: "HelveticaNeue-Light", size: 15)
        reportButt.frame = CGRect(x: 0, y: 0, width: 50, height: 44)
        reportButt.addTarget(self, action: #selector(reportButton(_:)), for: UIControlEvents.touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: reportButt)
        

        
        // Init AdMob
        initAdMobBanner()
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // Event Title
        self.title = "\(eventObj[EVENTS_TITLE]!)"
//        self.navigationController?.navigationBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "HelveticaNeue-Light", size: 15)!, NSForegroundColorAttributeName: UIColor.black]
        
        let imageFile = eventObj[EVENTS_IMAGE] as? PFFile
        imageFile?.getDataInBackground { (imageData, error) -> Void in
            if error == nil {
                if let imageData = imageData {
                    self.eventImage.image = UIImage(data:imageData)
                }}}
        
        
        // Description
        descrTxt.text = "\(eventObj[EVENTS_DESCRIPTION]!)"
        descrTxt.sizeToFit()
        
        
        let dayFormatter = DateFormatter()
        dayFormatter.dateFormat = "dd"
        let dayStr = dayFormatter.string(from: eventObj[EVENTS_START_DATE] as! Date)
        dayNrLabel.text = dayStr
        
        let weekdayFormatter = DateFormatter()
        weekdayFormatter.dateFormat = "EEEE"
        let weekdayStr = weekdayFormatter.string(from: eventObj[EVENTS_START_DATE] as! Date)
        weekDayLabel.text = weekdayStr
        
        let monthFormatter = DateFormatter()
        monthFormatter.dateFormat = "MMM"
        let monthStr = monthFormatter.string(from: eventObj[EVENTS_START_DATE] as! Date)
        monthLabel.text = monthStr
        
//        let yearFormatter = DateFormatter()
//        yearFormatter.dateFormat = "yyyy"
//        let yearStr = yearFormatter.string(from: eventObj[EVENTS_START_DATE] as! Date)
//        yearLabel.text = yearStr
        
        let startDateFormatter = DateFormatter()
        startDateFormatter.dateFormat = "MMM dd - hh:mm a"
        let startDateStr = startDateFormatter.string(from: eventObj[EVENTS_START_DATE] as! Date)
        let endDateFormatter = DateFormatter()
        endDateFormatter.dateFormat = "MMM dd - hh:mm a"
        let endDateStr = endDateFormatter.string(from: eventObj[EVENTS_END_DATE] as? Date ?? Date())
        
        startDateLabel.text = "\(startDateStr)"
        if endDateStr != "" {  endDateLabel.text = "\(endDateStr)"
        } else { endDateLabel.text = ""  }
        
        
        addToCalOutlet.contentEdgeInsets = UIEdgeInsetsMake(0,10,0,10)
        shareOutlet.contentEdgeInsets = UIEdgeInsetsMake(0,10,0,10)
        
        // If Event Passed Disable Button
//        let currentDate = Date()
//        if currentDate.isGreaterThanDate(eventObj[EVENTS_END_DATE] as! Date) {
//            addToCalOutlet.isEnabled = false
//            addToCalOutlet.setTitle("This event has passed", for: UIControlState())
//            registerOutlet.isEnabled = false
//            registerOutlet.setTitle("EVENT PASSED", for: UIControlState())
//        }
        
        // Cost
        costLabel.text = eventObj[EVENTS_COST] as? String ?? ""
        
        // URL
//        if eventObj[EVENTS_WEBSITE] != nil {
//            websiteLabel.text = "\(eventObj[EVENTS_WEBSITE]!)"
//        } else {  websiteLabel.text = ""  }
        
        // Locationa
        locationLabel.text = eventObj[EVENTS_LOCATION_NAME] as? String ?? ""
        addPinOnMap(locationLabel.text!.lowercased(), geoPoint: eventObj[EVENTS_LOCATION] as? PFGeoPoint)
        
        
        titleLbl.text = "\(eventObj[EVENTS_TITLE]!)".uppercased()
        
        detailsView.frame.origin.y = descrTxt.frame.origin.y + descrTxt.frame.size.height + 10
        containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width, height: detailsView.frame.origin.y + detailsView.frame.size.height)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    // Add PIN on Map
    func addPinOnMap(_ address: String, geoPoint: PFGeoPoint?) {
        mapView.delegate = self
        
        if mapView.annotations.count != 0 {
            annotation = mapView.annotations[0]
            mapView.removeAnnotation(annotation)
        }
        
        if let geoPoint = geoPoint{

            
            let pointAnnotation = MKPointAnnotation()
            pointAnnotation.title = "\(self.eventObj[EVENTS_TITLE]!)"
            pointAnnotation.coordinate = CLLocationCoordinate2D( latitude: geoPoint.latitude, longitude:geoPoint.longitude)

            
            pinView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: nil)
            self.mapView.centerCoordinate = pointAnnotation.coordinate
            self.mapView.addAnnotation(pinView.annotation!)
            
            region = MKCoordinateRegionMakeWithDistance(pointAnnotation.coordinate, 1000, 1000);
            self.mapView.setRegion(self.region, animated: true)
            self.mapView.regionThatFits(self.region)
            self.mapView.reloadInputViews()
            
            self.mapView.selectAnnotation(pointAnnotation, animated: true)


        }else{
            localSearchRequest = MKLocalSearchRequest()
            localSearchRequest.naturalLanguageQuery = address
            localSearch = MKLocalSearch(request: localSearchRequest)
            localSearch.start { (localSearchResponse, error) -> Void in
                self.pointAnnotation = MKPointAnnotation()
                self.pointAnnotation.title = "\(self.eventObj[EVENTS_TITLE]!)"
                if let localSearchResponse = localSearchResponse {
                self.pointAnnotation.coordinate = CLLocationCoordinate2D( latitude: localSearchResponse.boundingRegion.center.latitude, longitude:localSearchResponse.boundingRegion.center.longitude)
                }
                self.pinView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
                self.mapView.centerCoordinate = self.pointAnnotation.coordinate
                self.mapView.addAnnotation(self.pinView.annotation!)
                self.region = MKCoordinateRegionMakeWithDistance(self.pointAnnotation.coordinate, 1000, 1000);
                self.mapView.setRegion(self.region, animated: true)
                self.mapView.regionThatFits(self.region)
                self.mapView.reloadInputViews()
            }
        }
    }
    
    // Custom PIN
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKind(of: MKPointAnnotation.self) {
            let reuseID = "CustomPinAnnotationView"
            var annotView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
            if annotView == nil {
                annotView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
                annotView!.canShowCallout = true
                
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
                imageView.image =  UIImage(named: "mapPin")
                imageView.center = annotView!.center
                imageView.contentMode = UIViewContentMode.scaleAspectFill
                annotView!.addSubview(imageView)
                
                let rightButton = UIButton(type: UIButtonType.custom)
                rightButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                rightButton.layer.cornerRadius = rightButton.bounds.size.width/2
                rightButton.clipsToBounds = true
                rightButton.setImage(UIImage(named: "openInMaps"), for: UIControlState())
                annotView!.rightCalloutAccessoryView = rightButton
            }
            return annotView
        }
        
        return nil
    }
    
    
    // Open iOS Maps
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        annotation = view.annotation
        let coordinate = annotation.coordinate
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapitem = MKMapItem(placemark: placemark)
        mapitem.name = annotation.title!
        mapitem.openInMaps(launchOptions: nil)
    }
    
    
    // Add Event iOS Calendar
    @IBAction func addToCalButt(_ sender: AnyObject) {
        let eventStore = EKEventStore()
        
        let status = EKEventStore.authorizationStatus(for: EKEntityType.event)
        switch status {
        case .authorized: insertEvent(eventStore)
        case .denied: print("Access denied")
        case .notDetermined:
            
            eventStore.requestAccess(to: .event, completion: { (granted, error) -> Void in
                if granted { self.insertEvent(eventStore)
                } else { print("Access denied")  }
            })
            
        default: print("Case Default")
        }
    }
    
    func insertEvent(_ store: EKEventStore) {
        let calendars = store.calendars(for: EKEntityType.event)
        
        for calendar in calendars {
            
            if calendar.title == "Calendar" || calendar.title == "Calendario" {
                let startDate = eventObj[EVENTS_START_DATE] as? Date ?? Date()
                let endDate = eventObj[EVENTS_END_DATE] as? Date ?? startDate
                let event = EKEvent(eventStore: store)
                event.calendar = calendar
                event.title = "\(eventObj[EVENTS_TITLE]!)"
                event.startDate = startDate
                event.endDate = endDate
                
                var error: NSError?
                let result: Bool
                do {
                    try store.save(event, span: EKSpan.thisEvent)
                    result = true
                } catch let error1 as NSError {
                    error = error1
                    result = false
                }
                
                simpleAlert("Added to your iOS Calendar")
                
                if result == false {
                    if let theError = error {
                        print("ERROR: \(theError)")
                    }
                }
                
            }
            
        }
    }
    
    
    
    
    
    // Share Button
    @IBAction func shareButt(_ sender: AnyObject) {
        
        let messageStr  = "\(NSLocalizedString("View this event", comment:"")): \(eventObj[EVENTS_TITLE]!) en #\(APP_NAME)"
        let shareItems = [messageStr, eventImage.image!] as [Any]
        
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            // iPad
            let popOver = UIPopoverController(contentViewController: activityViewController)
            popOver.present(from: CGRect.zero, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        } else {
            // iPhone
            present(activityViewController, animated: true, completion: nil)
        }
    }
    
    

    
    // Open URL
    @IBAction func openLinkButt(_ sender: AnyObject) {
        let webURL = URL(string: "\(eventObj[EVENTS_WEBSITE]!)")
        UIApplication.shared.openURL(webURL!)
    }
    
    
    // Register Button
    @IBAction func registerButt(_ sender: AnyObject) {
        let webURL = URL(string: "\(eventObj[EVENTS_WEBSITE]!)")
        UIApplication.shared.openURL(webURL!)
    }
    
    
    
    // Report
    func reportButton(_ sender: UIButton) {
        
        let messageStr = "<font size = '1' color= '#222222' style = 'font-family: 'HelveticaNeue'>Hi,<br>Check the following Event since it seems it contains inappropriate/offensive contents:<br><br>Event Title: <strong>\(eventObj[EVENTS_TITLE]!)</strong><br>Event ID: <strong>\(eventObj.objectId!)</strong><br><br>Cheers.</font>"
        
        let mailComposer = MFMailComposeViewController()
        mailComposer.mailComposeDelegate = self
        mailComposer.setSubject("Reporting inappropriate contents on an Event")
        mailComposer.setMessageBody(messageStr, isHTML: true)
        mailComposer.setToRecipients([REPORT_EMAIL_ADDRESS])
        
        if MFMailComposeViewController.canSendMail() {
            present(mailComposer, animated: true, completion: nil)
        } else {
            simpleAlert("Your device cannot send emails. Please configure an email address into Settings > Mail, Contacts, Calendars.")
        }
    }

    func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
        
        var resultMess = ""
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            resultMess = "Mail cancelled"
        case MFMailComposeResult.saved.rawValue:
            resultMess = "Mail saved"
        case MFMailComposeResult.sent.rawValue:
            resultMess = "Mail sent!"
        case MFMailComposeResult.failed.rawValue:
            resultMess = "Something went wrong with sending Mail, try again later."
        default:break
        }
        
        simpleAlert(resultMess)
        dismiss(animated: false, completion: nil)
    }
    
    
    
    
    // Back Button
    func backButton(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    

    
    // AdMob Banner
    func initAdMobBanner() {
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: 320, height: 50)
        adMobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    
    // Hide
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2, y: view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
    }
    
    // Show
    func showBanner(_ banner: UIView) {
        UIView.beginAnimations("showBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2,
                                  y: view.frame.size.height - banner.frame.size.height - 47,
                                  width: banner.frame.size.width, height: banner.frame.size.height);
        UIView.commitAnimations()
        banner.isHidden = false
    }
    
    // AdMob available
    func adViewDidReceiveAd(_ view: GADBannerView!) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // No banner available
    func adView(_ view: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }
    
    

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onBack(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
