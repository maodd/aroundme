//
//  Appdelegate.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit
import Parse


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {

    var window: UIWindow?
    var locationManager: CLLocationManager!
    var currentLocation:CLLocation?

    // shared stores for map vc opened by different view.
    var storesArray = [PFObject]()
    
    static func sharedDelegate() -> AppDelegate {
        return (UIApplication.shared.delegate as? AppDelegate)!
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        // TabBar Icon Colors
        let customGreenColor = UIColor(red: 34/255.0, green: 154/255.0, blue: 136/255.0, alpha: 1)
        UITabBar.appearance().tintColor = customGreenColor
        //Set Colors
        
        for name in UIFont.familyNames {
            print(name)
            if let nameString = name as? String
            {
                print(UIFont.fontNames(forFamilyName: nameString))
            }
        }

        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:customGreenColor,
                                                        NSFontAttributeName: UIFont(name: "gobCL-Heavy", size: 20)!]
        
//        UINavigationBar.appearance().barTintColor =
        UINavigationBar.appearance().tintColor = customGreenColor
 
        
        // Config Parse
        let configuration = ParseClientConfiguration {
            $0.applicationId = PARSE_APP_KEY
            $0.clientKey = PARSE_CLIENT_KEY
            $0.server = "https://parseapi.back4app.com"
            $0.isLocalDatastoreEnabled = true
        }
     
        
        Parse.enableLocalDatastore()

        Parse.initialize(with: configuration)

        
        
        // FUse One Signal as Push Services
        _ = OneSignal(launchOptions: launchOptions, appId: ONESIGNAL_APP_ID, handleNotification: nil)
        OneSignal.defaultClient().enable(inAppAlertNotification: true)
        
        
         getCurrentLocation()
        
        return true
    }

    
    // For Push Notifications
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let installation = PFInstallation.current()
//        {
        installation.setDeviceTokenFrom(deviceToken)
        installation.saveInBackground(block: nil)
//        }
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        if error._code == 3010 {
            print("Push notifications are not supported in the iOS Simulator.")
        } else {
            print("application:didFailToRegisterForRemoteNotificationsWithError: %@", error)
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        PFPush.handle(userInfo)
        if application.applicationState == UIApplicationState.inactive {
            PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(inBackground: userInfo, block: nil)
        }
    }

    
    func applicationWillResignActive(_ application: UIApplication) {
      }

    func applicationDidEnterBackground(_ application: UIApplication) {
      }

    func applicationWillEnterForeground(_ application: UIApplication) {
      }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
  
    func getCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        if locationManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
            locationManager.requestAlwaysAuthorization()
        }
        
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to Get Your Location")
        
        
    }
    //func locationManager(_ manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
    //    locationManager.stopUpdatingLocation()
    //
    //    currentLocation = newLocation
    //
    //    // Query for Nearby Places
    //    queryStores(currentLocation!)
    //}
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
//        locationManager.stopUpdatingLocation()
        
        let distance:CLLocationDistance  = 100.0
        
        if self.currentLocation == nil
            || distance < (self.currentLocation?.distance(from: locations.last!))!  {
            
            currentLocation = locations.last!
            
            
            print("got new location \(currentLocation)")
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LocationFix"), object: nil)

        }
        
        
        
    }
    



}

