//
//  Home.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit
import Parse
import GoogleMobileAds
import AudioToolbox
import Kingfisher



// MARK: - CUSTOM STORE CELL
class StoreCell: UITableViewCell {

    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var favoriteButton: UIButton!
    
    
}



// Classes
class Home: UIViewController, UITableViewDelegate, UITableViewDataSource, GADBannerViewDelegate, CategoriesDelegate, UIGestureRecognizerDelegate
{

    @IBOutlet weak var storesTableView: UITableView!
    var refreshControl: UIRefreshControl?

    var favortieOnlyMode = false
    
    // AdMob Banner
    let adMobBannerView = GADBannerView()
    
    // Vars
    var storesArray = [PFObject]()
    var favoriteStoreList = [PFObject]()

    
//    var favArray = [PFObject]()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
override func viewWillAppear(_ animated: Bool) {
   
    // Pull to Refresh
    if  refreshControl != nil {
        refreshControl!.removeFromSuperview()
        refreshControl = nil
    }
   
    
    refreshControl =  UIRefreshControl()
    
    refreshControl?.tintColor = UIColor.gray
    refreshControl?.addTarget(self, action: #selector(refreshTB), for: .valueChanged)
    storesTableView.addSubview(refreshControl!)
    

    
    if category == "\(storeCategories[0])" {
        category = ""
    }
    
    
    
    print("CATEGORY: \(category)\nDISTANCE: \(distance)")
    
    self.loadData()
   
    
    if (self.navigationController?.isNavigationBarHidden)! {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    }
    
    func loadData() {
        
        queryStores(AppDelegate.sharedDelegate().currentLocation)
    }
    
override func viewDidLoad() {
    super.viewDidLoad()
    
//    cacheAllData()
    if favortieOnlyMode {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.title = "Mis Lugares Favoritos"
    }
    
    // Title
//    self.title = "ParqueMet"
    
    
    // AdMob Banners
    initAdMobBanner()
    
    self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    //    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    
    
    NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "LocationFix"), object: nil, queue: nil) { (notif) in
        self.loadData()
    }

    
}

 
    
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if(navigationController!.viewControllers.count > 1){
            return true
        }
        return false
    }
    
    
    func queryFavStores() {
        showHUD()
        
        let query = PFQuery(className: FAVORITES_CLASS_NAME)
        query.whereKey(FAVORITES_INSTALLATION_POINTER, equalTo: PFInstallation.current())
        query.findObjectsInBackground { (objects, error)-> Void in
            if let favArray = objects {
                
                    self.favoriteStoreList = favArray.map({ (favorite) -> PFObject in
                        (favorite[FAVORITES_STORE_POINTER] as? PFObject)!
                    })
                    
                    
                    
              
                self.hideHUD()
            } else {
                self.simpleAlert("\(error?.localizedDescription)")
                self.hideHUD()
            }
        
            self.storesTableView.reloadData()
        
        }
    }
    

// Pull to Refresh
func refreshTB () {
    
        
    if (refreshControl?.isRefreshing)! {
//        let formatter = DateFormatter()
//        let date = Date()
//        formatter.dateFormat = "MMM d, h:mm a"
//        let title = "Last update: \(formatter.string(from: date))"
//        let attrsDictionary = NSDictionary(object: UIColor.red, forKey: NSForegroundColorAttributeName as NSCopying)
//        let attributedTitle = NSAttributedString(string: title, attributes: attrsDictionary as? [String : AnyObject]);
//        refreshControl?.attributedTitle = attributedTitle
        

        refreshControl?.endRefreshing()
    }
    
    queryStores(AppDelegate.sharedDelegate().currentLocation)
}
    
    
   
   
    
// Show Nearby Places
func queryStores(_ location:CLLocation?) {
    showHUD()
    let currentGeoPoint = PFGeoPoint(location: location)
    print("CURRENT GEO POINT: \(currentGeoPoint)")
    
    let query = PFQuery(className: STORES_CLASS_NAME)
    // TODO: enable this after test is done.
//    query.whereKey(STORES_LOCATION, nearGeoPoint: currentGeoPoint, withinKilometers: distance)
    query.whereKey(STORES_IS_PENDING, equalTo: false)
    if category != "" {
        
        query.whereKey(STORES_CATEGORY, equalTo: category)
    }
    query.limit = 1000
    
    
    
    query.findObjectsInBackground { (objects, error)-> Void in
        
    
        
        if error == nil {
            
            self.storesArray = objects!
            
            AppDelegate.sharedDelegate().storesArray = self.storesArray
            
            if let location = location {
                self.storesArray = self.storesArray.sorted(by: { (storeA, storeB) -> Bool in
                    
                    
                    
                    guard let pA = storeA[STORES_LOCATION] as? PFGeoPoint
                        , let pB = storeB[STORES_LOCATION] as? PFGeoPoint else {
                            return true
                            
                    }
                    
                    let userLoc = PFGeoPoint(location: AppDelegate.sharedDelegate().currentLocation)
                    return pA.distanceInKilometers(to: userLoc) < pB.distanceInKilometers(to: userLoc)
                })
            
            }
            self.storesTableView.reloadData()
            
            self.queryFavStores()
            
            
            self.hideHUD()
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}
}
    
    
    
    
// TableView
func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}
    
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if  favortieOnlyMode {
        return self.favoriteStoreList.count
    }
    return storesArray.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "StoreCell", for: indexPath) as! StoreCell
    
    let store = favortieOnlyMode  ?
          favoriteStoreList[indexPath.row]
        :
          storesArray[indexPath.row]
    
    
    store.fetchIfNeededInBackground { (obj, error) in
        
        if let storeClass = obj {
        
            cell.nameLabel.text = "\(storeClass[STORES_NAME]!)".uppercased()
            cell.addressLabel.text =  storeClass[STORES_CATEGORY] as? String ?? "N/A"
            //    cell.categoryLabel.text = "\(storeClass[STORES_CATEGORY]!)"
            
            if let storeGeoPoint = storeClass[STORES_LOCATION] as? PFGeoPoint{
                
            let storeLoc = CLLocation(latitude: storeGeoPoint.latitude, longitude: storeGeoPoint.longitude)
                
            if let location = AppDelegate.sharedDelegate().currentLocation {
                let distanceInKM: CLLocationDistance = storeLoc.distance(from: location) / 1000
                cell.distanceLabel.text = String(format: "%.1f KM", distanceInKM)
            }else{
                
                cell.distanceLabel.text = "-- KM"
            }
            }else{
                
                cell.distanceLabel.text = "-- KM"
            }
            cell.distanceLabel.layer.cornerRadius = 6
            
            if let imageFile = storeClass[STORES_IMAGE] as? PFFile {
//            imageFile?.getDataInBackground { (imageData, error) -> Void in
//                if error == nil {
//                    if let imageData = imageData {
//                        
//                        cell.storeImage.image = UIImage(data:imageData)
// 
//                    }}
//            }
            cell.storeImage.kf.setImage(with: URL(string: (imageFile.url)!),
                                        placeholder: nil,
                                        options: [.transition(ImageTransition.fade(1))],
                                        progressBlock: nil,
                                        completionHandler: nil)
            }else{
                cell.storeImage.image = nil
            }
        
            if self.favortieOnlyMode {
                cell.favoriteButton.isSelected = false
            }else{
                let match = self.favoriteStoreList.filter({ (favorite) -> Bool in
                    favorite.objectId == storeClass.objectId
                })
                
                
                if match.count > 0 {
                    cell.favoriteButton.isSelected = false
                }else{
                    cell.favoriteButton.isSelected = true
                }
                
            }
            //    }
            
            if let category = storeClass[STORES_CATEGORY] as? String {
                
                cell.categoryImageView.image = UIImage(named: category.getIconImageNameFromCategory() )
                cell.distanceView.backgroundColor = category.getIconTintColorForCategory()
            }
   
        }
        
        
        
        }
        return cell
    }
    
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 243
    }
    
    
// Cell Places
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var storeClass = PFObject(className: STORES_CLASS_NAME)
    
    if self.favortieOnlyMode {
        storeClass = favoriteStoreList[indexPath.row]
    }else{
        storeClass = storesArray[(indexPath as NSIndexPath).row]
    }
    
    let cell = tableView.cellForRow(at: indexPath) as! StoreCell
    
    let aVC = storyboard?.instantiateViewController(withIdentifier: "StoreDetails") as! StoreDetails
    aVC.storeObj = storeClass
    aVC.sourceCell = cell
    
    navigationController?.pushViewController(aVC, animated: true)
    
}

    
    
// Account Button
@IBAction func accountButt(_ sender: AnyObject) {
    if PFUser.current() != nil {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "Account") as! Account
        navigationController?.pushViewController(aVC, animated: true)
        
    } else {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
        present(aVC, animated: true, completion: nil)
    }
}
    
    
    
    
// AdMob Banners
func initAdMobBanner() {
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: view.frame.size.height, width: 320, height: 50)
        adMobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    // Hide
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2, y: view.frame.size.height - banner.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
    }
    
    // Show
    func showBanner(_ banner: UIView) {
        UIView.beginAnimations("showBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2, y: view.frame.size.height - banner.frame.size.height - 44, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = false
    }
    
    // Available
    func adViewDidReceiveAd(_ view: GADBannerView!) {
        showBanner(adMobBannerView)
    }
    
    // No banner available
    func adView(_ view: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        hideBanner(adMobBannerView)
    }
    
    
    func categoryChanged() {
        
        if category == "\(storeCategories[0])" {
            category = ""
        }
        
        
        queryStores(AppDelegate.sharedDelegate().currentLocation)
    
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? Categories {
            vc.delegate = self
        }
        
    }
    
    @IBAction func favButt(_ sender: AnyObject) {
        guard let button = sender as? UIButton else {fatalError()}
        
        var view = button.superview
        
        while !(view?.isKind(of: StoreCell.self))! {
            
            view = view?.superview
            
        }
        
        let cell = view as! StoreCell
        
        guard let indexPath = self.storesTableView.indexPath(for: cell) else {fatalError()}
        
        
        
      
        
        let storeObj = favortieOnlyMode ?
            self.favoriteStoreList[(indexPath.row)]
            : self.storesArray[(indexPath.row)]
        
        
        
        
        //    if PFUser.current() == nil {
        //        let alert = UIAlertView(title: APP_NAME,
        //            message: "You must be logged in to favorite this store!",
        //            delegate: self,
        //            cancelButtonTitle: "Cancel",
        //            otherButtonTitles: "Login")
        //        alert.show()
        //
        //
        //    } else {
        
        if (self.favoriteStoreList.filter({ (store) -> Bool in
            store.objectId == storeObj.objectId
        }).count == 0 ) {
            favoriteStore(storeObj: storeObj)
        }else{
            unfavoriteStore(storeObj: storeObj)
        }
    
        cell.favoriteButton.isSelected = !cell.favoriteButton.isSelected
    }

    func favoriteStore(storeObj: PFObject) {
        
        let query = PFQuery(className: FAVORITES_CLASS_NAME)
        query.whereKey(FAVORITES_INSTALLATION_POINTER, equalTo: PFInstallation.current())
        query.whereKey(FAVORITES_STORE_POINTER, equalTo: storeObj)
        query.countObjectsInBackground(block: { (count, error) in
           
            if count == 0 {
                let favClass = PFObject(className: FAVORITES_CLASS_NAME)
                favClass[FAVORITES_INSTALLATION_POINTER] = PFInstallation.current()
                favClass[FAVORITES_STORE_POINTER] = storeObj
                
                favClass.saveInBackground(block: { (succ, error) in
                    if error == nil {
                        //                        self.simpleAlert("You've added this store to your Favorites!")
                        //                        self.favoriteOutlet.isSelected = true
                    } else { self.simpleAlert("\(error!.localizedDescription)")
                    }})
            }
            
        })
        
        

    }
    func unfavoriteStore(storeObj: PFObject) {
        let query = PFQuery(className: FAVORITES_CLASS_NAME)
        query.whereKey(FAVORITES_INSTALLATION_POINTER, equalTo: PFInstallation.current())
        query.whereKey(FAVORITES_STORE_POINTER, equalTo: storeObj)
        query.findObjectsInBackground { (objects, error)-> Void in
            if error == nil {
                if let objects = objects {
                    PFObject.deleteAll(inBackground: objects, block: { (succeed, error) in
                        if error == nil {
                            //                            self.simpleAlert(NSLocalizedString("You've removed this store from your Favorites!", comment: ""))
                            
                            self.queryFavStores()
                            
                            
                            
                        } else { self.simpleAlert("\(error!.localizedDescription)")
                        }
                    })
                }
                
                
                
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
            }}
        
        //    }
    }

}
