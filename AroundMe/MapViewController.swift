//
//  MapViewController.swift
//  AroundMe
//
//  Created by Frank Mao on 2016-11-29.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit
import Mapbox
import Parse
import MapKit

class AMPointAnnotation : MGLPointAnnotation, MKAnnotation {
    var category:String!
    var storeId:String!
}

//
// MGLAnnotationView subclass
class CustomAnnotationView: MGLAnnotationView {
    
    var image:UIImage?
    var imageView:UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
       
        
        if imageView == nil {
            imageView = UIImageView(frame: self.bounds)
            imageView.image = image
            
            self.addSubview(imageView)
        }

        self.backgroundColor = UIColor.clear
        
        // Force the annotation view to maintain a constant size when the map is tilted.
//        scalesWithViewingDistance = false
        
        // Use CALayer’s corner radius to turn this view into a circle.
//        layer.cornerRadius = frame.width / 2
//        layer.borderWidth = 2
//        layer.borderColor = UIColor.white.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Animate the border width in/out, creating an iris effect.
//        let animation = CABasicAnimation(keyPath: "borderWidth")
//        animation.duration = 0.1
//        layer.borderWidth = selected ? frame.width / 8 : 2
//        layer.borderColor = selected ? UIColor.lightGray.cgColor : UIColor.white.cgColor
//        layer.add(animation, forKey: "borderWidth")
    }
}


class MapViewController: UIViewController
//, MKMapViewDelegate{
, MGLMapViewDelegate, CategoriesDelegate {

    @IBOutlet var mapView: MGLMapView!
//    var storesArray = [PFObject]()
    var pointAnnotation:AMPointAnnotation!
    var pinView:MKPinAnnotationView!
    var region: MKCoordinateRegion!
    var annotation:MKAnnotation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.attributionButton.setImage(UIImage(named:"current-location-icon"), for: .normal)
        
        // Do any additional setup after loading the view.
//        mapView.delegate = self
        
//        let point = MGLPointAnnotation()
//        point.coordinate = CLLocationCoordinate2D(latitude: 45.52258, longitude: -122.6732)
//        point.title = "Voodoo Doughnut"
//        point.subtitle = "22 SW 3rd Avenue Portland Oregon, U.S.A."
//        
//        mapView.addAnnotation(point)
        mapView.styleURL = NSURL(string: "mapbox://styles/mapbox/outdoors-v9") as URL!
        mapView.delegate = self
        
        addPinsOnMap()
        
//        region = MKCoordinateRegionMakeWithDistance(self.pointAnnotation.coordinate, 1000, 1000);
//        mapView.centerCoordinate = self.pointAnnotation.coordinate
//        mapView.setRegion(self.region, animated: true)
//        mapView.regionThatFits(self.region)
        
        
        
        mapView.reloadInputViews()
        


        
        mapViewFitsAllAnnotations()

    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
         self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    }
    
    func addPinsOnMap() {
        if let annotaions = mapView.annotations {
            mapView.removeAnnotations(annotaions)
        }
        
        for store in AppDelegate.sharedDelegate().storesArray {
            if let geoPoint = store[STORES_LOCATION] as? PFGeoPoint{
                let latitude:CLLocationDegrees = geoPoint.latitude
                let longitude:CLLocationDegrees = geoPoint.longitude
                self.addPinOnMap(latitude, long: longitude,
                             title: store[STORES_NAME] as! String ,
                             category: store[STORES_CATEGORY] as! String,
                             storeId: store.objectId!
                             )
            }
        }
    }
    
    func mapViewFitsAllAnnotations() {
        var allAnnMapRect = MKMapRectNull
        if let annotation = self.mapView.annotations {
            for annotation in annotation {
          
                
                let thisAnnMapPoint = MKMapPointForCoordinate(annotation.coordinate)
                let thisAnnMapRect = MKMapRectMake(thisAnnMapPoint.x, thisAnnMapPoint.y, 1, 1)
                allAnnMapRect = MKMapRectUnion(allAnnMapRect, thisAnnMapRect)
            }
            
        }
        
        //Set inset (blank space around all annotations) as needed...
        //These numbers are in screen CGPoints...
        let edgeInset = UIEdgeInsetsMake(20, 50, 20, 50)
        
        let sw = MKCoordinateForMapPoint(MKMapPointMake(allAnnMapRect.origin.x, allAnnMapRect.origin.y + allAnnMapRect.size.height) )
        let ne = MKCoordinateForMapPoint(MKMapPointMake(allAnnMapRect.origin.x + allAnnMapRect.size.width, allAnnMapRect.origin.y) )
//        let center = CLLocationCoordinate2D(latitude: -33.406853, longitude: -70.616932)
//        MKMapPointMake(allAnnMapRect.origin.x + allAnnMapRect.size.width/2.0, allAnnMapRect.origin.y + allAnnMapRect.size.height/2.0))
//        self.mapView.setVisibleMapRect(allAnnMapRect, edgePadding: edgeInset, animated: true)
        let bounds = MGLCoordinateBounds(sw: sw, ne: ne)
        self.mapView.setVisibleCoordinateBounds(bounds, edgePadding: edgeInset, animated: true)
//        self.mapView.setCenter (center, zoomLevel: 10, animated: false)
    }

    func addPinOnMap(_ lat: CLLocationDegrees, long: CLLocationDegrees, title: String, category: String, storeId: String) {
     
        
        pointAnnotation = AMPointAnnotation()
        pointAnnotation.category = category
        pointAnnotation.storeId = storeId
        pointAnnotation.title = title
        pointAnnotation.coordinate = CLLocationCoordinate2D(
            latitude: lat,
            longitude: long
        )
        
        pinView = MKPinAnnotationView(annotation: pointAnnotation , reuseIdentifier: nil)
  
        mapView.addAnnotation(pinView.annotation as! MGLAnnotation )
        

        
    }
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        // This example is only concerned with point annotations.
        guard annotation is MGLPointAnnotation else {
            return nil
        }
        
   
        
        // Use the point annotation’s longitude value (as a string) as the reuse identifier for its view.
        let reuseIdentifier = "\(annotation.coordinate.longitude)"
        
        // For better performance, always try to reuse existing annotations.
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) as? CustomAnnotationView
        
        // If there’s no reusable annotation view available, initialize a new one.
        if annotationView == nil {
            annotationView = CustomAnnotationView(reuseIdentifier: reuseIdentifier)
            annotationView!.frame = CGRect(x:0, y:0, width:40, height:40)
            
        
            
            annotationView?.image =  UIImage(named: "mapicon_informaciones")!
            if let annotation = annotation as? AMPointAnnotation {
                
                
                
                if annotation.category == "Accesos" {
                    annotationView?.image =  UIImage(named: "mapicon_access")!
                }
                else if annotation.category == "Atractivos" {
                    annotationView?.image =  UIImage(named: "mapicon_atractivos")!
                }
                else if annotation.category == "Baños y Camarines" {
                    annotationView?.image =  UIImage(named: "mapicon_bañoscamarines")!
                }
                else if annotation.category == "Cumbres" {
                    annotationView?.image =  UIImage(named: "mapicon_cumbres")!
                }
                else if annotation.category == "Estacionamientos" {
                    annotationView?.image =  UIImage(named: "mapicon_estacionamientos")!
                }
                else if annotation.category == "Hitos Temáticos" {
                    annotationView?.image =  UIImage(named: "mapicon_hitostematicos")!
                }
                else if annotation.category == "Informaciones" {
                    annotationView?.image =  UIImage(named: "mapicon_informaciones")!
                }
                else if annotation.category == "Juegos Infantiles" {
                    annotationView?.image =  UIImage(named: "mapicon_juegosinfantiles")!
                }
                else if annotation.category == "Miradores" {
                    annotationView?.image =  UIImage(named: "mapicon_miradores")!
                }
                else if annotation.category == "Plazas y Jardines" {
                    annotationView?.image =  UIImage(named: "mapicon_plazasyjardines")!
                }
                else if annotation.category == "Punto Limpio" {
                    annotationView?.image =  UIImage(named: "mapicon_reciclaje")!
                }
                else if annotation.category == "Red de Parque Urbanos" {
                    annotationView?.image =  UIImage(named: "mapicon_redparqueurbanos")!
                }
                else if annotation.category == "Restaurantes y quioscos" {
                    annotationView?.image =  UIImage(named: "mapicon_restaurantsyquiscos")!
                }
                else if annotation.category == "Sendero Ciclista" {
                    annotationView?.image =  UIImage(named: "mapicon_senderosciclistas")!
                }
                else if annotation.category == "Sendero Peatonal" {
                    annotationView?.image =  UIImage(named: "mapicon_senderospeatonal")!
                }
                else if annotation.category == "Zonas Deportivas" {
                    annotationView?.image =  UIImage(named: "mapicon_sonasdeportivas")!
                }
                else if annotation.category == "Zonas de Picnic" {
                    annotationView?.image =  UIImage(named: "mapicon_zonaspicnic")!
                }
            }

            
            
            // Set the annotation view’s background color to a value determined by its longitude.
            let hue = CGFloat(annotation.coordinate.longitude) / 100
            annotationView!.backgroundColor = UIColor(hue: hue, saturation: 0.5, brightness: 1, alpha: 1)
            
            
            
           
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MGLMapView, rightCalloutAccessoryViewFor annotation: MGLAnnotation) -> UIView? {
        
        let rightButton = UIButton(type: UIButtonType.custom)
        rightButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        rightButton.layer.cornerRadius = rightButton.bounds.size.width/2
        rightButton.clipsToBounds = true
        rightButton.setImage(UIImage(named: "openInMaps"), for: UIControlState())
        
        
        return rightButton
    }
    
    func mapView(_ mapView: MGLMapView, annotation: MGLAnnotation, calloutAccessoryControlTapped control: UIControl) {
        // Hide the callout view.
        mapView.deselectAnnotation(annotation, animated: false)
        
//        UIAlertView(title: annotation.title!!, message: "A lovely (if touristy) place. \((annotation as! AMPointAnnotation).storeId )", delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK").show()
        
        let storeId =  (annotation as! AMPointAnnotation).storeId!
        let query = PFQuery(className: STORES_CLASS_NAME)
        query.whereKey("objectId", equalTo: storeId)
        query.getFirstObjectInBackground(block:  { (object, error) in
            if let store = object {
                let aVC = self.storyboard?.instantiateViewController(withIdentifier: "StoreDetails") as! StoreDetails
                aVC.storeObj = store
                //        aVC.sourceCell = cell
                
                self.navigationController?.pushViewController(aVC, animated: true)

                
            }
        })
        
       
    }

    // Custom Pin Annotation
//    func mapView(_ mapView: MKMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
//        // Handle custom annotations.
//        if annotation.isKind(of: AMPointAnnotation.self) {
//            
//            let reuseID = "CustomPinAnnotationView"
//            var annotView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID)
//            
//            if annotView == nil {
//                annotView = MGLAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
//                annotView!.canShowCallout = true
//                
//                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
//                
//                imageView.image =  UIImage(named: "mapPin")
//                if let annotation = annotation as? AMPointAnnotation {
//                    if annotation.category == "Zonas de Picnic" {
//                        imageView.image =  UIImage(named: "icon-gondola")
//                    }else if annotation.category == "Programas y recreación" {
//                        imageView.image =  UIImage(named: "icon-track")
//                    }
//                }
//                
//
//                imageView.center = annotView!.center
//                imageView.contentMode = UIViewContentMode.scaleAspectFill
//                annotView!.addSubview(imageView)
//                
//                let rightButton = UIButton(type: UIButtonType.custom)
//                rightButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
//                rightButton.layer.cornerRadius = rightButton.bounds.size.width/2
//                rightButton.clipsToBounds = true
//                rightButton.setImage(UIImage(named: "openInMaps"), for: UIControlState())
//                annotView!.rightCalloutAccessoryView = rightButton
//            }
//            return annotView
//        }
//        
//        return nil
//    }
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
 
    // Open Native Maps
    private func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        annotation = view.annotation
        let coordinate = annotation.coordinate
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
        let mapitem = MKMapItem(placemark: placemark)
        mapitem.name = annotation.title!
        mapitem.openInMaps(launchOptions: nil)
    }
   
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        
//        self.navigationController?.isNavigationBarHidden = false
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onBackButtonTapped(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
//    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
//        // Always try to show a callout when an annotation is tapped.
//        return true
//    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? Categories {
            vc.delegate = self
        }
 
        
        
    }
    
    func categoryChanged() {
        
        if category == "\(storeCategories[0])" {
            category = ""
        }
        
       
        queryStores(nil)
       
        
    }
    
    func queryStores(_ location:CLLocation?) {
        showHUD()
        let currentGeoPoint = PFGeoPoint(location: location)
        print("CURRENT GEO POINT: \(currentGeoPoint)")
        
        let query = PFQuery(className: STORES_CLASS_NAME)
        // TODO: enable this after test is done.
        //    query.whereKey(STORES_LOCATION, nearGeoPoint: currentGeoPoint, withinKilometers: distance)
        query.whereKey(STORES_IS_PENDING, equalTo: false)
        if category != "" {
            
            query.whereKey(STORES_CATEGORY, equalTo: category)
        }
        query.limit = 1000
        
        query.findObjectsInBackground { (objects, error)-> Void in
            
            
            
            if error == nil {
                
                
                
                AppDelegate.sharedDelegate().storesArray = objects!
                
                self.addPinsOnMap()
                
                self.hideHUD()
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
                self.hideHUD()
            }}
    }
}
