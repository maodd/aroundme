//
//  NewsDetails.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit
import Parse
import GoogleMobileAds
import AudioToolbox
import Kingfisher


class NewsDetails: UIViewController, GADBannerViewDelegate
{

    @IBOutlet weak var containerScrollView: UIScrollView!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newsTxt: UITextView!
    @IBOutlet weak var dateLabel: UILabel!    
    @IBOutlet weak var shareOutlet: UIButton!
    // AdMob Banner
    let adMobBannerView = GADBannerView()

    
    // Vars
    var newsObj = PFObject(className: NEWS_CLASS_NAME)
    
    func handleSwipeFrom() {
        self.backButt()
    }
    
override func viewDidLoad() {
        super.viewDidLoad()
    
    self.title = "Top News"
    let gesture = UISwipeGestureRecognizer(target: self, action: #selector(StoreDetails.handleSwipeFrom))
    gesture.direction = [.left , .right]
    
    containerScrollView.addGestureRecognizer(gesture)
    // Back Button Item
//    let backB = UIButton(type: UIButtonType.custom)
//    backB.adjustsImageWhenHighlighted = false
//    backB.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
//    backB.setTitle("BACK", for: UIControlState())
//    backB.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
//    backB.setTitleColor(UIColor(red: 238/255, green: 0/255, blue: 98/255, alpha: 1), for: UIControlState())
//    backB.setTitleColor(UIColor(red: 238/255, green: 0/255, blue: 98/255, alpha: 1), for: UIControlState.highlighted)
//    backB.addTarget(self, action: #selector(backButt), for: .touchUpInside)
//    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backB)
    
    shareOutlet.contentEdgeInsets = UIEdgeInsetsMake(0,10,0,10)
    
    // News Details
    let imageFile = newsObj[NEWS_IMAGE] as? PFFile
//    imageFile?.getDataInBackground(block: { (imageData, error) -> Void in
//        if error == nil {
//            if let imageData = imageData {
//                self.newsImage.image = UIImage(data:imageData)
//    }}})

    self.newsImage.kf.setImage(with: URL(string: (imageFile?.url)!),
                                placeholder: nil,
                                options: [.transition(ImageTransition.fade(1))],
                                progressBlock: nil,
                                completionHandler: nil)
    titleLabel.text = "\(newsObj[NEWS_TITLE]!)".uppercased()
    newsTxt.text = "\(newsObj[NEWS_TEXT]!)"
    newsTxt.sizeToFit()
    
    containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width,
                        height: newsTxt.frame.origin.y + newsTxt.frame.size.height + 160)

    let date = newsObj.createdAt
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = "dd MMM, yyyy"
    dateLabel.text = dateFormat.string(from: date!)
    
    
    self.navigationController?.setNavigationBarHidden(true, animated: true)
    // AdMob Banner
    //initAdMobBanner()
}

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.navigationController?.isNavigationBarHidden = false
    }
 
    
// Share Button
@IBAction func shareStoreButt(_ sender: AnyObject) {
        let messageStr  = "\(NSLocalizedString("Check out", comment:"")): \(newsObj[NEWS_TITLE]!) en #\(APP_NAME)"
        let img = newsImage.image!
        
        let shareItems = [messageStr, img] as [Any]
        
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            // iPad
            let popOver = UIPopoverController(contentViewController: activityViewController)
            popOver.present(from: CGRect.zero, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        } else {
            // iPhone
            present(activityViewController, animated: true, completion: nil)
        }
}
    

    
// Back Button
func backButt() {
    _ = navigationController?.popViewController(animated: true)
}
    
    
    
// AdMob Banners
    func initAdMobBanner() {
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: view.frame.size.height, width: 320, height: 50)
        adMobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    // Hide
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2, y: view.frame.size.height - banner.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
    }
    
    // Show
    func showBanner(_ banner: UIView) {
        UIView.beginAnimations("showBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2, y: view.frame.size.height - banner.frame.size.height - 44, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = false
    }
    
    // Available
    func adViewDidReceiveAd(_ view: GADBannerView!) {
        showBanner(adMobBannerView)
    }
    
    // No banner available
    func adView(_ view: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        hideBanner(adMobBannerView)
    }
    

    @IBAction func onBack(_ sender: AnyObject) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
