//
//  Categories.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit
import Parse


// MARK: - CUSTOM CATEGORY CELL
class CatCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var catLabel: UILabel!
    @IBOutlet weak var catImageView: UIImageView!
    
}
class CatCell: UITableViewCell {
    
    @IBOutlet weak var catLabel: UILabel!
}

protocol CategoriesDelegate {
    func categoryChanged()
}


// Classes
class Categories: UIViewController, UITableViewDelegate, UITableViewDataSource
    ,UICollectionViewDelegate, UICollectionViewDataSource
{
    
    @IBOutlet weak var catCollectionView: UICollectionView!
    @IBOutlet weak var catTableView: UITableView!
    @IBOutlet weak var distanceSlider: UISlider!
    @IBOutlet weak var distanceLabel: UILabel!

    
    var delegate: CategoriesDelegate?
    
    var cellSize = CGSize()
    
    func handleSwipeFrom() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
override func viewDidLoad() {
        super.viewDidLoad()
    
//    if !(self.navigationController?.isNavigationBarHidden)! {
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//    }
    
    let gesture = UISwipeGestureRecognizer(target: self, action: #selector(StoreDetails.handleSwipeFrom))
    gesture.direction = [.left , .right]
    
    catCollectionView.addGestureRecognizer(gesture)
    
    // Setup
    if  category.characters.count == 0 {
        category = "\(storeCategories[0])"
    }
    distance = 35
    distanceSlider.value = 35
    distanceSlider.setThumbImage(UIImage(named: "sliderThumb"), for: UIControlState())
    distanceSlider.setThumbImage(UIImage(named: "sliderThumb"), for: .highlighted)
 
    catTableView.tableFooterView = UIView(frame: .zero)
    
    catCollectionView.delegate = self
    
    if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
        // iPhone
        cellSize = CGSize(width: view.frame.size.width / 2 - 10, height: 130)
    } else  {
        // iPad
        cellSize = CGSize(width: 150, height: 130)
    }
}

 
    
func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}
    
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return storeCategories.count
}
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CatCell", for: indexPath) as! CatCell
    
    cell.catLabel.text = "\(storeCategories[(indexPath as NSIndexPath).row])"
    
    // Color Background
    let bgColorView = UIView()
    bgColorView.backgroundColor = UIColor(red: 162.0/255.0, green: 149.0/255.0, blue: 146.0/255.0, alpha: 0.35)
    cell.selectedBackgroundView = bgColorView
    
    
return cell
}
    
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 50
}
    
    
// Cells
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let cell = tableView.cellForRow(at: indexPath) as! CatCell
    
    cell.catLabel.textColor = UIColor.black
    category = "\(cell.catLabel.text!)"
}
    
    
    
// Slider
@IBAction func distanceChanged(_ sender: UISlider) {
    distanceLabel.text = "Distance: \( Int(sender.value) ) Km"
    distance = Double(sender.value)
}
    
    
    
// Abort Button
@IBAction func dismissButt(_ sender: AnyObject) {
    
    if  let delegate = delegate {
        delegate.categoryChanged()
    }
    
//    dismiss(animated: true, completion: nil)
    _ = self.navigationController?.popViewController(animated: true)
    
}
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // View Delegates
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storeCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatCollectionViewCell", for: indexPath) as! CatCollectionCell
        
        let catName = storeCategories[(indexPath as NSIndexPath).row]
        
        cell.catLabel.text = catName
        cell.catImageView.image = UIImage(named: catName == category ? catName.getFilterIconOnImageNameFromCategory()
            : catName.getFilterIconOffImageNameFromCategory()
        )
//        cell.catImageView.layer.borderColor = catName.getIconTintColorForCategory().cgColor
//        cell.catImageView.layer.borderWidth = 1.0
        
        cell.catLabel.textColor = catName == category ? catName.getIconTintColorForCategory() : UIColor.lightGray
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
         return cellSize
    }
    
    // Details
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        category = storeCategories[indexPath.row]
        
        if  let delegate = delegate {
            delegate.categoryChanged()
        }
        
//        dismiss(animated: true, completion: nil)
        self.dismissButt(self)
    }
}
