//
//  PostReview.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit
import Parse


class PostReview: UIViewController, UITextViewDelegate
{

    @IBOutlet var containerScrollView: UIScrollView!
    @IBOutlet var revTxt: UITextView!
    @IBOutlet var starsView: UIView!
    @IBOutlet var starButtons: [UIButton]!
    
    var starNr = 0
    var storeToReview = PFObject(className: STORES_CLASS_NAME)
    
    
    

override func viewDidLoad() {
        super.viewDidLoad()

    self.title = "Post a Review"
    
    
    // Send Button Item
    let butt = UIButton(type: UIButtonType.custom)
    butt.adjustsImageWhenHighlighted = false
    butt.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
    butt.setTitle("SEND", for: UIControlState())
    butt.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
    butt.setTitleColor(UIColor(red: 238/255, green: 0/255, blue: 98/255, alpha: 1), for: UIControlState())
    butt.setTitleColor(UIColor(red: 238/255, green: 0/255, blue: 98/255, alpha: 1), for: UIControlState.highlighted)
    butt.addTarget(self, action: #selector(sendReviewButt), for: .touchUpInside)
    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: butt)
    

    // Left Button Item
//    let cancelButt = UIButton(type: UIButtonType.custom)
//    cancelButt.adjustsImageWhenHighlighted = false
//    cancelButt.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
//    cancelButt.setTitle("BACK", for: UIControlState())
//    cancelButt.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
//    cancelButt.setTitleColor(UIColor(red: 238/255, green: 0/255, blue: 98/255, alpha: 1), for: UIControlState())
//    butt.setTitleColor(UIColor(red: 238/255, green: 0/255, blue: 98/255, alpha: 1), for: UIControlState.highlighted)
//    cancelButt.addTarget(self, action: #selector(cancelButton), for: .touchUpInside)
//    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: cancelButt)

    
    // Layout
    revTxt.layer.cornerRadius = 2
    starNr = 0
    
    
    // Heart Button
    for butt in starButtons {
        butt.setBackgroundImage(UIImage(named: "emptyStar"), for: UIControlState())
        butt.addTarget(self, action: #selector(starButtTapped(_:)), for: UIControlEvents.touchUpInside)
    }
    
    
    // What device use?
    if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
        containerScrollView.isScrollEnabled = false
    } else {
        containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width, height: 600)
    }
}

    
    
// Heart Button
func starButtTapped (_ sender: UIButton) {
    let button = sender as UIButton
    
    for i in 0..<starButtons.count {
        starButtons[i].setBackgroundImage(UIImage(named: "emptyStar"), for: UIControlState())
    }
    
    starNr = button.tag + 1
    print("STARS: \(starNr)")
    for star in 0..<starNr {
        starButtons[star].setBackgroundImage(UIImage(named: "fullStar"), for: UIControlState())
    }
}
    
    
    
// Feedback Button
func sendReviewButt() {
    showHUD()
    revTxt.resignFirstResponder()
    
    if revTxt.text == "" {
        self.simpleAlert("The review messagge must not be empty")
        hideHUD()
        
        
    } else {
        let reviewClass = PFObject(className: REVIEWS_CLASS_NAME)
    
        reviewClass[REVIEWS_STARS] = starNr as Int
        reviewClass[REVIEWS_TEXT] = revTxt.text!
        reviewClass[REVIEWS_USER_POINTER] = PFUser.current()!
        reviewClass[REVIEWS_STORE_POINTER] = storeToReview
        
        reviewClass.saveInBackground(block: { (succ, error) in
            if error == nil {
                self.simpleAlert("Thank you, review submit successfully")
                self.hideHUD()
                self.storeToReview.incrementKey(STORES_REVIEWS, byAmount: 1)
                self.storeToReview.saveInBackground()
                _ = self.navigationController?.popViewController(animated: true)
            
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
                self.hideHUD()
        }})
    }
}
    
    
    
    
// Cancell Button
func cancelButton() {
    _ = navigationController?.popViewController(animated: true)
}

    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
