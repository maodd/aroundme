//
//  Favorites.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit
import Parse
import GoogleMobileAds
import AudioToolbox


class Favorites: Home
{

//    @IBOutlet weak var favTableView: UITableView!
//    var refreshControl :UIRefreshControl?
//
//    // Vars
//    var favArray = [PFObject]()
//    var locationManager: CLLocationManager!
//    var currentLocation:CLLocation?
    
    
    
override func viewDidLoad() {
        super.viewDidLoad()
    
    
    
    self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    
    // Get Location
    getCurrentLocation()
    
    
    // Query
//    if PFUser.current() != nil {
    
//    } else {
//        favArray.removeAll()
//        favTableView.reloadData()
//    }
    
 
                                           
}

    override func viewWillAppear(_ animated: Bool) {
        
        // Pull to Refresh
        if  refreshControl != nil {
            refreshControl!.removeFromSuperview()
            refreshControl = nil
        }
        
        
        refreshControl =  UIRefreshControl()
        
        // Refresh Button
        refreshControl?.tintColor = UIColor.gray
        refreshControl?.addTarget(self, action: #selector(refreshTB), for: .valueChanged)
        storesTableView.addSubview(refreshControl!)

        
        
        
        if category == "\(storeCategories[0])" {
            category = ""
        }
        
        
        
        print("CATEGORY: \(category)\nDISTANCE: \(distance)")
        
        queryFavStores()
    }
    
 
 
    override func queryFavStores() {
        showHUD()
        
        let query = PFQuery(className: FAVORITES_CLASS_NAME)
        query.whereKey(FAVORITES_INSTALLATION_POINTER, equalTo: PFInstallation.current())
        query.findObjectsInBackground { (objects, error)-> Void in
            if error == nil {
                self.favArray = objects!
                
                
                self.storesTableView.reloadData()
                self.hideHUD()
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
                self.hideHUD()
            }}
    }

    
 
    
    
override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}
    
override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return favArray.count
}
    
override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "StoreCell", for: indexPath) as! StoreCell
        
    var favClass = PFObject(className: FAVORITES_CLASS_NAME)
    favClass = favArray[(indexPath as NSIndexPath).row]
    var storePointer = favClass[FAVORITES_STORE_POINTER] as! PFObject
    
    let currentGeoPoint = PFGeoPoint(location: currentLocation)
    
//    do { storePointer = try storePointer.fetchIfNeeded() } catch {}
    storePointer.fetchIfNeededInBackground { (store, error) in
        if let store = store {
            
            

            print("CURRENT GEO POINT: \(currentGeoPoint)")
            
            storePointer = store
            
            cell.nameLabel.text = storePointer[STORES_NAME] as? String ?? "N/A"
            cell.addressLabel.text = storePointer[STORES_ADDRESS] as? String ?? "N/A"
            cell.categoryLabel.text = "\(storePointer[STORES_CATEGORY]!)"
            
            let storeGeoPoint = storePointer[STORES_LOCATION] as! PFGeoPoint
            let storeLoc = CLLocation(latitude: storeGeoPoint.latitude, longitude: storeGeoPoint.longitude)
            let distanceInKM: CLLocationDistance = storeLoc.distance(from: self.currentLocation!) / 1000
            cell.distanceLabel.text = String(format: "%.2f Km", distanceInKM)
            cell.distanceLabel.layer.cornerRadius = 6
            
            
            let imageFile = storePointer[STORES_IMAGE] as? PFFile
            imageFile?.getDataInBackground { (imageData, error) -> Void in
                if error == nil {
                    if let imageData = imageData {
                        cell.storeImage.image = UIImage(data:imageData)
                    }}}
        }
    }
    
    
    
        
return cell
}
override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 215
}
    
    
// Cells
override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var favClass = PFObject(className: FAVORITES_CLASS_NAME)
    favClass = favArray[(indexPath as NSIndexPath).row]
    var storePointer = favClass[FAVORITES_STORE_POINTER] as! PFObject
    do { storePointer = try storePointer.fetchIfNeeded() } catch {}
    
    let aVC = storyboard?.instantiateViewController(withIdentifier: "StoreDetails") as! StoreDetails
    aVC.storeObj = storePointer
    aVC.isFromFavorites = true
    navigationController?.pushViewController(aVC, animated: true)
}
    
  
    
// Swipe Left to Remove Favorites
func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
}
func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == UITableViewCellEditingStyle.delete {
        
            var favClass = PFObject(className: FAVORITES_CLASS_NAME)
            favClass = favArray[(indexPath as NSIndexPath).row]
            favClass.deleteInBackground(block: { (succ, error) in
                if error == nil {
                    self.favArray.remove(at: (indexPath as NSIndexPath).row)
                    tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
                    
                } else {
                    self.simpleAlert("\(error!.localizedDescription)")
            }})
    }
}
    

    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
