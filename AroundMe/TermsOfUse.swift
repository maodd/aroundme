//
//  TermsOfUse.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//


import UIKit

class TermsOfUse: UIViewController {

    @IBOutlet var webView: UIWebView!
    

override func viewDidLoad() {
        super.viewDidLoad()
    
    let url = Bundle.main.url(forResource: "terms", withExtension: "html")
    webView.loadRequest(URLRequest(url: url!))

}

    
@IBAction func dismissButt(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
}
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
