//
//  Home.swift
//  Ultimate Events
//
//  Created by Greenfield
//  Copyright (c) 2016 Greenfield.com. All rights reserved.
//


import UIKit
import Parse
import GoogleMobileAds
import AudioToolbox
import Kingfisher


class Events: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, GADBannerViewDelegate
{
    
    @IBOutlet var eventsCollView: UICollectionView!
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTxt: UITextField!
    @IBOutlet var searchCityTxt: UITextField!
    @IBOutlet weak var searchOutlet: UIBarButtonItem!
    
    var refreshControl: UIRefreshControl?
    
    //AdMob Banner
    var adMobBannerView = GADBannerView()
    
    
    // Vars
    var eventsArray = [PFObject]()
    var cellSize = CGSize()
    var searchViewIsVisible = false
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
//        self.navigationItem.title = "Actividades"
        
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
            // iPhone
            cellSize = CGSize(width: view.frame.size.width-16, height: 232)
        } else  {
            // iPad
            cellSize = CGSize(width: 550, height: 232)
        }
        
        
        // Ini AdMob
        initAdMobBanner()
        
        
        let installation = PFInstallation.current()
//        {
            installation.addUniqueObject("Events", forKey: "channels")
            installation.saveInBackground()
//        }
        
        
        // Parse Query
        queryLatestEvents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Pull to Refresh
        if  refreshControl != nil {
            refreshControl!.removeFromSuperview()
            refreshControl = nil
        }
        
        
        refreshControl =  UIRefreshControl()
        
        refreshControl?.tintColor = UIColor.gray
        refreshControl?.addTarget(self, action: #selector(refreshTB), for: .valueChanged)
        eventsCollView.addSubview(refreshControl!)
        
        
        if (self.navigationController?.isNavigationBarHidden)! {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
        

    }
    
    // Pull to Refresh
    func refreshTB () {
        self.queryLatestEvents()
        
        if (refreshControl?.isRefreshing)! {
            //        let formatter = DateFormatter()
            //        let date = Date()
            //        formatter.dateFormat = "MMM d, h:mm a"
            //        let title = "Last update: \(formatter.string(from: date))"
            //        let attrsDictionary = NSDictionary(object: UIColor.red, forKey: NSForegroundColorAttributeName as NSCopying)
            //        let attributedTitle = NSAttributedString(string: title, attributes: attrsDictionary as? [String : AnyObject]);
            //        refreshControl?.attributedTitle = attributedTitle
            refreshControl?.endRefreshing()
        }
    }
    // Last Events
    func queryLatestEvents() {
        showHUD()
        
        let query = PFQuery(className: EVENTS_CLASS_NAME)
        query.whereKey(EVENTS_IS_PENDING, equalTo: false)
        query.whereKey(EVENTS_START_DATE, greaterThan: NSDate())
        query.order(byAscending: EVENTS_START_DATE)
        query.limit = limitForRecentEventsQuery
        query.findObjectsInBackground { (objects, error)-> Void in
            
            self.refreshControl?.endRefreshing()
            
            if error == nil {
                self.eventsArray = objects!
                self.eventsCollView.reloadData()
                self.hideHUD()
                
            } else {
                self.simpleAlert("\(error!.localizedDescription)")
                self.hideHUD()
            }}
        
    }
    
    
    
    // View Delegates
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return eventsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventCell", for: indexPath) as! EventCell
        
        var eventsClass = PFObject(className: EVENTS_CLASS_NAME)
        eventsClass = eventsArray[(indexPath as NSIndexPath).row]
        
        if let imageFile = eventsClass[EVENTS_IMAGE] as? PFFile {
//        imageFile?.getDataInBackground { (imageData, error) -> Void in
//            if error == nil {
//                if let imageData = imageData {
//                    cell.eventImage.image = UIImage(data:imageData)
//                }}}
        cell.eventImage.kf.setImage(with: URL(string: (imageFile.url)!),
                                    placeholder: nil,
                                    options: [.transition(ImageTransition.fade(1))],
                                    progressBlock: nil,
                                    completionHandler: nil)
        }else{
            cell.eventImage.image = nil
        }
        
        let dayFormatter = DateFormatter()
        dayFormatter.dateFormat = "dd"
        let dayStr = dayFormatter.string(from: eventsClass[EVENTS_START_DATE] as! Date)
        cell.dayNrLabel.text = dayStr
        
        let monthFormatter = DateFormatter()
        monthFormatter.dateFormat = "EEEE"
        let monthStr = monthFormatter.string(from: eventsClass[EVENTS_START_DATE] as! Date)
        cell.monthLabel.text = monthStr
        
        let yearFormatter = DateFormatter()
        yearFormatter.dateFormat = "MMM"
        let yearStr = yearFormatter.string(from: eventsClass[EVENTS_START_DATE] as! Date)
        cell.yearLabel.text = yearStr
        
        
        // Event Title
        cell.titleLbl.text = "\(eventsClass[EVENTS_TITLE]!)".uppercased()
        
        // Event Location
        cell.locationLabel.text = (eventsClass[EVENTS_LOCATION_NAME]) as? String ?? "" //.uppercased()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    
    // Details
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var eventsClass = PFObject(className: EVENTS_CLASS_NAME)
        eventsClass = eventsArray[(indexPath as NSIndexPath).row]
        
        let edVC = storyboard?.instantiateViewController(withIdentifier: "EventDetails") as! EventDetails
        edVC.eventObj = eventsClass
        navigationController?.pushViewController(edVC, animated: true)
    }
    
    
    // Refresh Button
    @IBAction func refreshButt(_ sender: AnyObject) {
        queryLatestEvents()
        self.title = "UltimateEvents"
    }
    
    
    
    
    // AdMob Banners
    func initAdMobBanner() {
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: 320, height: 50)
        adMobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    
    // Hide
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        // Hide the banner moving it below the bottom of the screen
        banner.frame = CGRect(x: 0, y: self.view.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
        
    }
    
    // Show
    func showBanner(_ banner: UIView) {
        UIView.beginAnimations("showBanner", context: nil)
        
        // Move the banner on the bottom of the screen
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2, y: view.frame.size.height - banner.frame.size.height - 47,
                                  width: banner.frame.size.width, height: banner.frame.size.height);
        
        UIView.commitAnimations()
        banner.isHidden = false
        
    }
    
    
    // AdMob banner available
    func adViewDidReceiveAd(_ view: GADBannerView!) {
        print("AdMob loaded!")
        showBanner(adMobBannerView)
    }
    
    // No banner available
    func adView(_ view: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        print("AdMob Can't load ads right now, they'll be available later \n\(error)")
        hideBanner(adMobBannerView)
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   

}
