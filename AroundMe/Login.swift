//
//  Login.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//


import UIKit
import Parse

class Login: UIViewController, UITextFieldDelegate, UIAlertViewDelegate
{
    
    @IBOutlet var containerScrollView: UIScrollView!
    @IBOutlet var usernameTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet weak var logo: UIImageView!
    
    
    
override func viewWillAppear(_ animated: Bool) {
    if PFUser.current() != nil {
        dismiss(animated: true, completion: nil)
    }
}
override func viewDidLoad() {
        super.viewDidLoad()
        
        // Layouts
        let width = containerScrollView.frame.size.width
        containerScrollView.contentSize = CGSize(width: width, height: 550)
    
        containerScrollView.setNeedsLayout()
    
        // Placeholder Colors
         let color = UIColor.lightGray
         usernameTxt.attributedPlaceholder = NSAttributedString(string: "your email address", attributes: [NSForegroundColorAttributeName: color])
         passwordTxt.attributedPlaceholder = NSAttributedString(string: "password", attributes: [NSForegroundColorAttributeName: color])
}
    
    
// Login Button
@IBAction func loginButt(_ sender: AnyObject) {
    dismissKeyboard()
    showHUD()
    
    PFUser.logInWithUsername(inBackground: usernameTxt.text!, password:passwordTxt.text!) { (user, error) -> Void in
        if user != nil {
            self.dismiss(animated: true, completion: nil)
            hudView.removeFromSuperview()
            
        } else {
            let alert = UIAlertView(title: APP_NAME,
                message: "\(error!.localizedDescription)",
                delegate: self,
                cancelButtonTitle: "Retry",
                otherButtonTitles: "Sign Up")
            alert.show()
            hudView.removeFromSuperview()
    }}
}

func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if alertView.buttonTitle(at: buttonIndex) == "Sign Up" {
            signupButt(self)
        }
        
        if alertView.buttonTitle(at: buttonIndex) == "Reset Password" {
            PFUser.requestPasswordResetForEmail(inBackground: "\(alertView.textField(at: 0)!.text!)")
            showNotifAlert()
        }
}

    
    
    
// SignUp Button
@IBAction func signupButt(_ sender: AnyObject) {
    let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUp") as! SignUp
    signupVC.modalTransitionStyle = .crossDissolve
    present(signupVC, animated: true, completion: nil)
}
    
    
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == usernameTxt  {  passwordTxt.becomeFirstResponder() }
    if textField == passwordTxt  {  passwordTxt.resignFirstResponder() }
    
return true
}
    
    
@IBAction func tapToDismissKeyboard(_ sender: UITapGestureRecognizer) {
    dismissKeyboard()
}
func dismissKeyboard() {
    usernameTxt.resignFirstResponder()
    passwordTxt.resignFirstResponder()
}
    
    
    
// Forgot Password Button
    @IBAction func forgotPasswButt(_ sender: AnyObject) {
        let alert = UIAlertView(title: APP_NAME,
            message: "Insert the email address used to register.",
            delegate: self,
            cancelButtonTitle: "Cancel",
            otherButtonTitles: "Reset Password")
        alert.alertViewStyle = UIAlertViewStyle.plainTextInput
        alert.show()
}

    
// Alert for Password reset
func showNotifAlert() {
    simpleAlert("You will receive an email shortly with a link to reset your password")
}

    
    
// Close Button
@IBAction func closeButt(_ sender: AnyObject) {
    dismiss(animated: true, completion: nil)
}
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


