//
//  WebViewController.swift
//  AroundMe
//
//  Created by Frank Mao on 2016-11-24.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit
import MessageUI

class WebViewController: UIViewController, MFMailComposeViewControllerDelegate, UIWebViewDelegate  {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Parquemet te ayuda"
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        // Do any additional setup after loading the view.
         webView.loadRequest(URLRequest(url: URL(string: "http://demo.allegra.ai/parquemet.html")!))
            
//        do {
//            let path = Bundle.main.path(forResource: "chat", ofType: "html")
//            let htmlString = try String(contentsOfFile: path!)
//
// 
//            webView.loadHTMLString(htmlString, baseURL: URL(string: "http://service.allegra.ai/"))
//
//        }
//        catch {/* error handling here */}
//                webView.loadRequest(URLRequest(url: URL(string: "http://www.lobarnechea.cl/new/")!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let height = webView.stringByEvaluatingJavaScript(from: "document.body.offsetHeight;")
        let javascript = String(format:"window.scrollBy(0, %@);", height!)
        webView.stringByEvaluatingJavaScript(from: javascript)
        
        
        let rect = CGRect(x:1, y:1, width:scrollView.contentSize.width - 1,
                          height:scrollView.contentSize.height - 1)
        scrollView.scrollRectToVisible(rect, animated: true)
        
       
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onMail(_ sender: Any) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["info@parquemet.cl"])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onSOS(_ sender: Any) {
        makePhoneCall(phoneNumber: "+56227301304")
    }

    @IBAction func onInformation(_ sender: Any) {
        makePhoneCall(phoneNumber: "+56227301331")
    }
    
    func makePhoneCall(phoneNumber: String) {
        if let url = NSURL(string: "tel://\(phoneNumber)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
}
