//
//  SignUp.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//



import UIKit
import Parse


class SignUp: UIViewController, UITextFieldDelegate
{
    
    @IBOutlet var containerScrollView: UIScrollView!
    @IBOutlet var usernameTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet weak var fullnameTxt: UITextField!
    
    
    
override func viewDidLoad() {
        super.viewDidLoad()
        
        // Layout
        containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width, height: 600)
        
         // Placeholder Colors
         let color = UIColor.lightGray
         usernameTxt.attributedPlaceholder = NSAttributedString(string: "your email address", attributes: [NSForegroundColorAttributeName: color])
         passwordTxt.attributedPlaceholder = NSAttributedString(string: "type a password", attributes: [NSForegroundColorAttributeName: color])
         fullnameTxt.attributedPlaceholder = NSAttributedString(string: "type your full name", attributes: [NSForegroundColorAttributeName: color])
}
    
    
@IBAction func tapToDismissKeyboard(_ sender: UITapGestureRecognizer) {
    dismissKeyboard()
}
    
func dismissKeyboard() {
    usernameTxt.resignFirstResponder()
    passwordTxt.resignFirstResponder()
    fullnameTxt.resignFirstResponder()
}
    
    
// SignUp Button
@IBAction func signupButt(_ sender: AnyObject) {
    showHUD()
        
    let userForSignUp = PFUser()
    userForSignUp.username = usernameTxt.text!.lowercased()
    userForSignUp.email = usernameTxt.text!.lowercased()
    userForSignUp.password = passwordTxt.text
    userForSignUp[USER_FULLNAME] = fullnameTxt.text
   
    userForSignUp.signUpInBackground { (succeeded, error) -> Void in
        if error == nil {
            self.dismiss(animated: true, completion: nil)
            self.hideHUD()
                
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}
}
    
    
    
func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == usernameTxt {  passwordTxt.becomeFirstResponder()  }
    if textField == passwordTxt {  fullnameTxt.becomeFirstResponder()  }
    if textField == fullnameTxt {  fullnameTxt.resignFirstResponder()  }
return true
}
    
    
    
// Back Button
@IBAction func backButt(_ sender: AnyObject) {
    dismiss(animated: true, completion: nil)
}
    
    
    
// Terms
@IBAction func touButt(_ sender: AnyObject) {
    let touVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsOfUse") as! TermsOfUse
    present(touVC, animated: true, completion: nil)
}
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}



