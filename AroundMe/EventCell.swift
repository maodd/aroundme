//
//  EventCell.swift
//  Ultimate Events
//
//  Created by Greenfield
//  Copyright (c) 2016 Greenfield.com. All rights reserved.
//


import UIKit

class EventCell: UICollectionViewCell {
    
    @IBOutlet var eventImage: UIImageView!
    @IBOutlet var dayNrLabel: UILabel!
    @IBOutlet var monthLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var costLabel: UILabel!
    
}
