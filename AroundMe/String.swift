//
//  String.swift
//  AroundMe
//
//  Created by Frank Mao on 2016-12-06.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit

extension String {
    func getFilterIconOnImageNameFromCategory() -> String {
 
        return getIconImageNameFromCategory() + "2"
    }

    func getFilterIconOffImageNameFromCategory() -> String {
        
        return getIconImageNameFromCategory() + "1"
    }

    
    func getIconImageNameFromCategory() -> String {
        
        switch self {
        case "Todos":
            return "select-all"
        case "Accesos":
            return "Category_access"
        case "Atractivos":
            return "Category_atractivos"
        case "Baños y Camarines":
            return "Category_bañoscamarines"
        case "Cumbres":
            return "Category_cumbres"
        case "Estacionamientos":
            return "Category_estacionamientos"
        case "Hitos Temáticos":
            return "Category_hitostematicos"
        case "Informaciones":
            return "Category_informaciones"
        case "Juegos Infantiles":
            return "Category_juegosinfantiles"
        case "Miradores":
            return "Category_miradores"
        case "Plazas y Jardines":
            return "Category_plazasyjardines"
        case "Punto Limpio":
            return "Category_reciclaje"
        case "Red de Parque Urbanos":
            return "Category_redparqueurbanos"
        case "Restaurantes y quioscos":
            return "Category_restaurantsyquiscos"
        case "Sendero Ciclista":
            return "Category_senderosciclistas"
        case "Sendero Peatonal":
            return "Category_senderospeatonal"
        case "Zonas Deportivas":
            return "Category_zonasdeportivas"
        case "Zonas de Picnic":
            return "Category_zonaspicnic"
        default:
            return "Category_informaciones"
        }
        
        
    }
    
    func getIconTintColorForCategory() -> UIColor {
        
        switch self {
        case "Todos":
            return UIColor(colorLiteralRed: 0, green: 149/255.0, blue: 131/255.0, alpha: 1)
        case "Accesos":
            return UIColor(colorLiteralRed: 0, green: 149/255.0, blue: 131/255.0, alpha: 1)
        case "Atractivos":
            return UIColor(colorLiteralRed: 247/255.0, green: 148/255.0, blue: 29/255.0, alpha: 1)
        case "Baños y Camarines":
            return UIColor(colorLiteralRed: 129/255.0, green: 130/255.0, blue: 132/255.0, alpha: 1)
        case "Cumbres":
            return UIColor(colorLiteralRed: 247/255.0, green: 148/255.0, blue: 29/255.0, alpha: 1)
        case "Estacionamientos":
            return UIColor(colorLiteralRed: 26/255.0, green: 100/255.0, blue: 155/255.0, alpha: 1)
        case "Hitos Temáticos":
            return UIColor(colorLiteralRed: 238/255.0, green: 28/255.0, blue: 37/255.0, alpha: 1)
        case "Informaciones":
            return UIColor(colorLiteralRed: 0, green: 149/255.0, blue: 131/255.0, alpha: 1)
        case "Juegos Infantiles":
            return UIColor(colorLiteralRed: 247/255.0, green: 148/255.0, blue: 29/255.0, alpha: 1)
        case "Miradores":
            return UIColor(colorLiteralRed: 100/255.0, green: 66/255.0, blue: 41/255.0, alpha: 1)
        case "Plazas y Jardines":
            return UIColor(colorLiteralRed: 142/255.0, green: 198/255.0, blue: 63/255.0, alpha: 1)
        case "Punto Limpio":
            return UIColor(colorLiteralRed: 129/255.0, green: 130/255.0, blue: 132/255.0, alpha: 1)
        case "Red de Parque Urbanos":
            return UIColor(colorLiteralRed: 83/255.0, green: 147/255.0, blue: 204/255.0, alpha: 1)
        case "Restaurantes y quioscos":
            return UIColor(colorLiteralRed: 247/255.0, green: 148/255.0, blue: 29/255.0, alpha: 1)
        case "Sendero Ciclista":
            return UIColor(colorLiteralRed: 238/255.0, green: 28/255.0, blue: 37/255.0, alpha: 1)
        case "Sendero Peatonal":
            return UIColor(colorLiteralRed: 254/255.0, green: 205/255.0, blue: 36/255.0, alpha: 1)
        case "Zonas Deportivas":
            return UIColor(colorLiteralRed: 247/255.0, green: 148/255.0, blue: 29/255.0, alpha: 1)
        case "Zonas de Picnic":
            return UIColor(colorLiteralRed: 247/255.0, green: 148/255.0, blue: 29/255.0, alpha: 1)
        default:
            return UIColor(colorLiteralRed: 247/255.0, green: 148/255.0, blue: 29/255.0, alpha: 1)
        }
        
        
    }
}
