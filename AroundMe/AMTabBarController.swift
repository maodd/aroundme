//
//  AMTabBarController.swift
//  AroundMe
//
//  Created by Frank Mao on 2016-11-18.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit

class AMTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    @IBOutlet var loadingView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        
        // Do any additional setup after loading the view.
       
        for tabBarItem in self.tabBar.items! {
            tabBarItem.title = ""
            tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
        }
        
        loadingView.frame = self.view.bounds
        self.view.addSubview(loadingView)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
            // Put your code which should be executed with a delay here
            self.loadingView.isHidden = true
            self.loadingView.removeFromSuperview()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool
    {
        
        if self.viewControllers?.index(of: viewController) == 2 {
            if let nvc = viewController as? UINavigationController,
                let vc = nvc.viewControllers.first as? Home {
                vc.favortieOnlyMode = true
                
                vc.queryFavStores()
            }
        }
        
        
        return true
    }
//    optional public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController)

    

}
