//
//  Date.swift
//  AroundMe
//
//  Created by Frank Mao on 2016-09-19.
//  Copyright © 2016 GF. All rights reserved.
//

// Extensions for Dates
extension Date {
    func isGreaterThanDate(_ dateToCompare : Date) -> Bool {
        var isGreater = false
        if self.compare(dateToCompare) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        return isGreater
    }
    
    func isLessThanDate(_ dateToCompare : Date) -> Bool {
        var isLess = false
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        return isLess
    }
    
    func isSameAsDate(_ dateToCompare : Date) -> Bool {
        var isEqualTo = false
        if self.compare(dateToCompare) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        return isEqualTo
    }
    
}
