//
//  StoreDetails.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit
import Parse
import MapKit
import GoogleMobileAds
import AudioToolbox
import MessageUI
import Kingfisher



// MARK: - CUSTOM REVIEW CELL
class ReviewCell: UITableViewCell {

    @IBOutlet weak var rTextLabel: UILabel!
    @IBOutlet weak var byDateLabel: UILabel!
    @IBOutlet weak var starsImage: UIImageView!
}



// Classes
class StoreDetails: UIViewController, UITabBarDelegate, UITableViewDataSource, MKMapViewDelegate, GADBannerViewDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate
{

    // Vars
    @IBOutlet weak var containerScrollView: UIScrollView!
    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingsLabel: UILabel!
    @IBOutlet weak var descriptionTxt: UITextView!
    @IBOutlet weak var description2Txt: UITextView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var phoneOutlet: UIButton!
    @IBOutlet weak var emailOutlet: UIButton!
    @IBOutlet weak var webOutlet: UIButton!
    @IBOutlet weak var favoriteOutlet: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var theMap: MKMapView!
    @IBOutlet weak var favOutlet: UIButton!
    @IBOutlet weak var shareOutlet: UIButton!
    @IBOutlet weak var reviewsTableView: UITableView!
    @IBOutlet weak var distanceView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    // AdMob Banner
    let adMobBannerView = GADBannerView()

    
    var sourceCell:StoreCell?
    
    // Vars
    var storeObj = PFObject(className: STORES_CLASS_NAME)
    var reviewsArray = [PFObject]()
    var favArray = [PFObject]()
    var isFromFavorites = Bool()
    var annotation:MKAnnotation!
    var localSearchRequest:MKLocalSearchRequest!
    var localSearch:MKLocalSearch!
    var localSearchResponse:MKLocalSearchResponse!
    var error:NSError!
    var pointAnnotation:MKPointAnnotation!
    var pinView:MKPinAnnotationView!
    var region: MKCoordinateRegion!
    
    
    func handleSwipeFrom() {
        self.backButt()
    }
    
override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    
    self.title = "ParqueMet"
    
    if !(self.navigationController?.isNavigationBarHidden)! {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    

    let gesture = UISwipeGestureRecognizer(target: self, action: #selector(StoreDetails.handleSwipeFrom))
    gesture.direction = [.left , .right]
        
    containerScrollView.addGestureRecognizer(gesture)
    
    if  let cell = sourceCell {
        
        self.categoryImageView.image = cell.categoryImageView.image
        
        
        
        self.distanceView.backgroundColor = cell.distanceView.backgroundColor
        self.distanceLabel.text = cell.distanceLabel.text

    }else{
        
        self.categoryImageView.image =  UIImage(named: (self.storeObj[STORES_CATEGORY] as? String ?? "") .getIconImageNameFromCategory() )
        self.distanceView.backgroundColor = (self.storeObj[STORES_CATEGORY] as? String ?? "").getIconTintColorForCategory()
        
        let storeGeoPoint = self.storeObj[STORES_LOCATION] as! PFGeoPoint
        let storeLoc = CLLocation(latitude: storeGeoPoint.latitude, longitude: storeGeoPoint.longitude)
        if let location = AppDelegate.sharedDelegate().currentLocation {
            let distanceInKM: CLLocationDistance = storeLoc.distance(from: location) / 1000
            self.distanceLabel.text = String(format: "%.1f KM", distanceInKM)
            
        }else{
            self.distanceLabel.text = "-- KM"
        }
    }
    
    // Add Review Button
//    let butt = UIButton(type: UIButtonType.custom)
//    butt.adjustsImageWhenHighlighted = false
//    butt.frame = CGRect(x: 0, y: 0, width: 75, height: 44)
//    butt.setTitle("+ REVIEW", for: UIControlState())
//    butt.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
//    butt.setTitleColor(UIColor(red: 238/255, green: 0/255, blue: 98/255, alpha: 1), for: UIControlState())
//    butt.setTitleColor(UIColor(red: 238/255, green: 0/255, blue: 98/255, alpha: 1), for: UIControlState.highlighted)
//    butt.addTarget(self, action: #selector(addReviewButt), for: .touchUpInside)
//    navigationItem.rightBarButtonItem = UIBarButtonItem(customView: butt)

    
    // Back Button
//    let backB = UIButton(type: UIButtonType.custom)
//    backB.adjustsImageWhenHighlighted = false
//    backB.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
//    backB.setTitle("BACK", for: UIControlState())
//    backB.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
//    backB.setTitleColor(UIColor(red: 238/255, green: 0/255, blue: 98/255, alpha: 1), for: UIControlState())
//    backB.setTitleColor(UIColor(red: 238/255, green: 0/255, blue: 98/255, alpha: 1), for: UIControlState.highlighted)
//    backB.addTarget(self, action: #selector(backButt), for: .touchUpInside)
//    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backB)
    
 
    
    // Store Details
    storeObj.fetchIfNeededInBackground { (store, error) in
        if let store = store {
            
            self.storeObj = store
            
            self.nameLabel.text = "\(self.storeObj[STORES_NAME]!)".uppercased().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
            if let imageFile = self.storeObj[STORES_IMAGE] as? PFFile {
//            imageFile?.getDataInBackground(block: { (imageData, error) -> Void in
//                if error == nil {
//                    if let imageData = imageData {
//                        self.storeImage.image = UIImage(data:imageData)
//                    }}})
            
            self.storeImage.kf.setImage(with: URL(string: (imageFile.url)!),
                                        placeholder: nil,
                                        options: [.transition(ImageTransition.fade(1))],
                                        progressBlock: nil,
                                        completionHandler: nil)
            }
            
            if self.isFromFavorites {
                self.favOutlet.isHidden = true
                self.shareOutlet.center.x = self.view.center.x
            }
            
            
            if self.storeObj[STORES_REVIEWS] != nil { self.ratingsLabel.text = "(\(self.storeObj[STORES_REVIEWS]!))"
            } else { self.ratingsLabel.text = "(0)" }
            
            self.descriptionTxt.text = self.storeObj[STORES_DESCRIPTION] as? String ?? ""
            self.descriptionTxt.sizeToFit()
            
            self.description2Txt.text = (self.storeObj[STORES_DESCRIPTION2]) as? String ?? ""
            self.description2Txt.sizeToFit()
            self.containerScrollView.setNeedsLayout()
            
            
            if self.storeObj[STORES_PHONE] != nil {
                self.phoneOutlet.setTitle("\(self.storeObj[STORES_PHONE]!)", for: UIControlState())
            } else {
                self.phoneOutlet.setTitle("N/A", for: UIControlState())
                self.phoneOutlet.isEnabled = false
            }
            
            self.emailOutlet.setTitle(self.storeObj[STORES_EMAIL] as? String ?? "N/A", for: UIControlState())
            
            if self.storeObj[STORES_WEBSITE] != nil {
                self.webOutlet.setTitle("\(self.storeObj[STORES_WEBSITE]!)", for: UIControlState())
            } else {
                self.webOutlet.setTitle("N/A", for: UIControlState())
                self.webOutlet.isEnabled = false
            }
            
            
            
            // Show Address on Map
//            self.addressLabel.text =  self.storeObj[STORES_ADDRESS] as? String ?? "N/A"
            self.addressLabel.text =  self.storeObj[STORES_CATEGORY] as? String ?? "N/A"
            if let geoPoint = self.storeObj[STORES_LOCATION] as? PFGeoPoint {
                let latitude:CLLocationDegrees = geoPoint.latitude
                let longitude:CLLocationDegrees = geoPoint.longitude
                self.addPinOnMap(latitude, long: longitude)
                
                 self.theMap.selectAnnotation(self.theMap.annotations.first!, animated: true)
            }
            
       
            
            
            self.bottomView.frame.origin.y = self.description2Txt.frame.size.height + self.description2Txt.frame.origin.y

        }
    }
    
    queryReviews()
    
    queryFavStores()
    
    // AdMob Banner
    initAdMobBanner()
    

}

    func queryFavStores() {
        showHUD()
        
        let query = PFQuery(className: FAVORITES_CLASS_NAME)
        query.whereKey(FAVORITES_INSTALLATION_POINTER, equalTo: PFInstallation.current())
        query.whereKey(FAVORITES_STORE_POINTER, equalTo: self.storeObj)
        query.countObjectsInBackground(block: { (count, error) in
            if error == nil && count > 0 {
                self.favoriteOutlet.isSelected = true
            }else{
                self.favoriteOutlet.isSelected = false
            }
                
                
        })
       
    }

    

// Add Pin Map
    func addPinOnMap(_ lat: CLLocationDegrees, long: CLLocationDegrees) {
        theMap.delegate = self
        
        if theMap.annotations.count != 0 {
            annotation = theMap.annotations[0]
            theMap.removeAnnotation(annotation)
        }
    
            pointAnnotation = MKPointAnnotation()
            pointAnnotation.title = "\(storeObj[STORES_NAME]!)"
            pointAnnotation.coordinate = CLLocationCoordinate2D(
                latitude: lat,
                longitude: long
            )
            
            pinView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
            theMap.centerCoordinate = self.pointAnnotation.coordinate
            theMap.addAnnotation(self.pinView.annotation!)
            
            region = MKCoordinateRegionMakeWithDistance(self.pointAnnotation.coordinate, 1000, 1000);
            theMap.setRegion(self.region, animated: true)
            theMap.regionThatFits(self.region)
            theMap.reloadInputViews()
    
}
    
// Custom Pin Annotation
func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Handle custom annotations.
        if annotation.isKind(of: MKPointAnnotation.self) {
            
            let reuseID = "CustomPinAnnotationView"
            var annotView = theMap.dequeueReusableAnnotationView(withIdentifier: reuseID)
            
            if annotView == nil {
                annotView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
                annotView!.canShowCallout = true
                
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
                imageView.image =  UIImage(named: "mapPin")
                imageView.center = annotView!.center
                imageView.contentMode = UIViewContentMode.scaleAspectFill
                annotView!.addSubview(imageView)
                
                let rightButton = UIButton(type: UIButtonType.custom)
                rightButton.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
                rightButton.layer.cornerRadius = rightButton.bounds.size.width/2
                rightButton.clipsToBounds = true
                rightButton.setImage(UIImage(named: "openInMaps"), for: UIControlState())
                annotView!.rightCalloutAccessoryView = rightButton
            }
        return annotView
        }
        
return nil
}
 
// Open Native Maps
func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
    annotation = view.annotation
    let coordinate = annotation.coordinate
    let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
    let mapitem = MKMapItem(placemark: placemark)
    mapitem.name = annotation.title!
    mapitem.openInMaps(launchOptions: nil)
}
    

   

    
// Query for Stores
func queryReviews() {
    reviewsArray.removeAll()
        
    let query = PFQuery(className: REVIEWS_CLASS_NAME)
    query.whereKey(REVIEWS_STORE_POINTER, equalTo: storeObj)
    query.findObjectsInBackground { (objects, error)-> Void in
        if error == nil {
            self.reviewsArray = objects!
            self.setHeightOfTableView()
            self.reviewsTableView.reloadData()
            
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
        }}
}
 

func setHeightOfTableView() {
    let tbHeight:CGFloat = 118 * CGFloat(reviewsArray.count)
    reviewsTableView.frame = CGRect(x: reviewsTableView.frame.origin.x, y: reviewsTableView.frame.origin.y, width: reviewsTableView.frame.size.width, height: tbHeight)
 
    bottomView.frame.size.height = tbHeight + reviewsTableView.frame.origin.y + 80
    
    // ContainerScrollView
    containerScrollView.contentSize = CGSize(width: containerScrollView.frame.size.width,
    height: description2Txt.frame.size.height + description2Txt.frame.origin.y + 20
//        + bottomView.frame.size.height
    )
}
    
    
func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}
   
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return reviewsArray.count
}
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
    
    var revClass = PFObject(className: REVIEWS_CLASS_NAME)
    revClass = reviewsArray[(indexPath as NSIndexPath).row]
    var userPointer = revClass[REVIEWS_USER_POINTER] as! PFUser
    do { userPointer = try  userPointer.fetchIfNeeded() } catch {}
    
    cell.rTextLabel.text = "\(revClass[REVIEWS_TEXT]!)"
    
    let date = revClass.createdAt
    let dateFormat = DateFormatter()
    dateFormat.dateFormat = "MMM dd yyyy"
    let dateStr = dateFormat.string(from: date!)
    
    cell.byDateLabel.text = "by \(userPointer[USER_FULLNAME]!) • \(dateStr)"
    cell.starsImage.image = UIImage(named: "\(revClass[REVIEWS_STARS]!)star")
    if revClass[REVIEWS_STARS] == nil {
        cell.starsImage.image = UIImage(named: "0star")
    }
    
        
return cell
}
 
private func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
    return 118
}
    
    
    
    
// Phone Call Button
@IBAction func phoneCallButt(_ sender: AnyObject) {
    let aURL = URL(string: "telprompt://\(storeObj[STORES_PHONE]!)")!
    if UIApplication.shared.canOpenURL(aURL) {
        UIApplication.shared.openURL(aURL)
    }
}
    
    
    
// Email Button
@IBAction func emailButton(_ sender: AnyObject) {
    let mailComposer = MFMailComposeViewController()
    mailComposer.mailComposeDelegate = self
    mailComposer.setToRecipients(["\(storeObj[STORES_EMAIL]!)"])
    mailComposer.setSubject("Contact request from \(APP_NAME)")
    mailComposer.setMessageBody("Hello,<br>", isHTML: true)
    
    if MFMailComposeViewController.canSendMail() {
        present(mailComposer, animated: true, completion: nil)
    } else {
        simpleAlert("Your device cannot send emails. Please configure an email address into Settings -> Mail, Contacts, Calendars.")
    }
}
    
// Email delegate
func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        var outputMessage = ""
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            outputMessage = "Mail cancelled"
        case MFMailComposeResult.saved.rawValue:
            outputMessage = "Mail saved"
        case MFMailComposeResult.sent.rawValue:
            outputMessage = "Mail sent"
        case MFMailComposeResult.failed.rawValue:
            outputMessage = "Something went wrong with sending Mail, try again later."
        default: break }
       
        simpleAlert(outputMessage)
        dismiss(animated: false, completion: nil)
}
    
    
    
    
// Website Button
@IBAction func webButt(_ sender: AnyObject) {
    let aURL = URL(string: "\(storeObj[STORES_WEBSITE]!)")
    UIApplication.shared.openURL(aURL!)
}
    


    
// Add Review Button
func addReviewButt() {
    if PFUser.current() != nil {
        let prVC = storyboard?.instantiateViewController(withIdentifier: "PostReview") as! PostReview
        prVC.storeToReview = storeObj
        navigationController?.pushViewController(prVC, animated: true)
    } else {
        let alert = UIAlertView(title: APP_NAME,
            message: "You must be logged in to post a review!",
            delegate: self,
            cancelButtonTitle: "Cancel",
            otherButtonTitles: "Login")
        alert.show()
    }
}
 

    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
    if alertView.buttonTitle(at: buttonIndex) == "Login" {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "Login") as! Login
        present(aVC, animated: true, completion: nil)
    }
}
   
    @IBAction func openInGoogleMap() {
        let storeGeoPoint = self.storeObj[STORES_LOCATION] as! PFGeoPoint
        
//        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.openURL(URL(string:String(format:
                "comgooglemapsurl://maps.google.com/?q=@@%f,%f", storeGeoPoint.latitude, storeGeoPoint.longitude))!)
//        } else {
//            print("Can't use comgooglemaps://");
//            let alert = UIAlertView(title: APP_NAME,
//                                    message: "Can't open Google map!",
//                                    delegate: self,
//                                    cancelButtonTitle: nil,
//                                    otherButtonTitles: "OK")
//            alert.show()
//        }

    }
    
// Favorites Button
@IBAction func favButt(_ sender: AnyObject) {
    favArray.removeAll()
    
//    if PFUser.current() == nil {
//        let alert = UIAlertView(title: APP_NAME,
//            message: "You must be logged in to favorite this store!",
//            delegate: self,
//            cancelButtonTitle: "Cancel",
//            otherButtonTitles: "Login")
//        alert.show()
//    
//    
//    } else {
    
        self.favoriteOutlet.isSelected = !self.favoriteOutlet.isSelected
    
        let query = PFQuery(className: FAVORITES_CLASS_NAME)
        query.whereKey(FAVORITES_INSTALLATION_POINTER, equalTo: PFInstallation.current())
        query.whereKey(FAVORITES_STORE_POINTER, equalTo: storeObj)
        query.findObjectsInBackground { (objects, error)-> Void in
            if error == nil {
                self.favArray = objects!
    
            
            // Remove Favorites
            if self.favArray.count != 0 {
                var favClass = PFObject(className: FAVORITES_CLASS_NAME)
                favClass = self.favArray[0]
                favClass.deleteInBackground(block: { (succ, error) in
                    if error == nil {
//                        self.simpleAlert("You've removed this store from your Favorites!")
//                        self.favoriteOutlet.isSelected = false
                    } else { self.simpleAlert("\(error!.localizedDescription)")
                }})
                
                
            // Favorite Places
            } else {
                let favClass = PFObject(className: FAVORITES_CLASS_NAME)
                favClass[FAVORITES_INSTALLATION_POINTER] = PFInstallation.current()
                favClass[FAVORITES_STORE_POINTER] = self.storeObj
                
                favClass.saveInBackground(block: { (succ, error) in
                    if error == nil {
//                        self.simpleAlert("You've added this store to your Favorites!")
//                        self.favoriteOutlet.isSelected = true
                    } else { self.simpleAlert("\(error!.localizedDescription)")
                }})
            }
            
        
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
        }}
    
//    }
}
    
    
    
    
// Share Place Button
@IBAction func shareStoreButt(_ sender: AnyObject) {
    let messageStr  = "\(NSLocalizedString("Check out", comment:"")): \(storeObj[STORES_NAME]!) en #\(APP_NAME)"
    let img = storeImage.image!
    
    let shareItems = [messageStr, img] as [Any]
    
    let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
    activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
    
    if UIDevice.current.userInterfaceIdiom == .pad {
        // iPad
        let popOver = UIPopoverController(contentViewController: activityViewController)
        popOver.present(from: CGRect.zero, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
    } else {
        // iPhone
        present(activityViewController, animated: true, completion: nil)
    }
}
    

    
// Back Button
func backButt() {
    _ = navigationController?.popViewController(animated: true)
}
    
    
   
// AdMob Banner
    func initAdMobBanner() {
        adMobBannerView.adSize =  GADAdSizeFromCGSize(CGSize(width: 320, height: 50))
        adMobBannerView.frame = CGRect(x: 0, y: view.frame.size.height, width: 320, height: 50)
        adMobBannerView.adUnitID = ADMOB_BANNER_UNIT_ID
        adMobBannerView.rootViewController = self
        adMobBannerView.delegate = self
        view.addSubview(adMobBannerView)
        
        let request = GADRequest()
        adMobBannerView.load(request)
    }
    
    // Hide
    func hideBanner(_ banner: UIView) {
        UIView.beginAnimations("hideBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2, y: view.frame.size.height - banner.frame.size.height, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = true
    }
    
    // Show
    func showBanner(_ banner: UIView) {
        UIView.beginAnimations("showBanner", context: nil)
        banner.frame = CGRect(x: view.frame.size.width/2 - banner.frame.size.width/2, y: view.frame.size.height - banner.frame.size.height - 48, width: banner.frame.size.width, height: banner.frame.size.height)
        UIView.commitAnimations()
        banner.isHidden = false
    }
    
    // Available
    func adViewDidReceiveAd(_ view: GADBannerView!) {
        showBanner(adMobBannerView)
    }
    
    // No banner available
    func adView(_ view: GADBannerView!, didFailToReceiveAdWithError error: GADRequestError!) {
        hideBanner(adMobBannerView)
    }
    

    @IBAction func onBack(_ sender: AnyObject) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

    }
    
  
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    
}
