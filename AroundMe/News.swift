//
//  News.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//

import UIKit
import Parse
import Kingfisher


class NewsCell: UITableViewCell {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
}



// Classes
class News: UIViewController, UITableViewDataSource, UITableViewDelegate
{

    @IBOutlet weak var newsTableView: UITableView!
    let refreshControl = UIRefreshControl()
    
    // Vars
    var newsArray = [PFObject]()
    
    
    
override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
    
    // Pull to refresh
    refreshControl.tintColor = UIColor.gray
    refreshControl.addTarget(self, action: #selector(refreshTB), for: .valueChanged)
    newsTableView.addSubview(refreshControl)
    
    
    self.navigationItem.title = "Noticias Parquemet"
    
    queryNews()
}

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
 
// Refresh
func refreshTB () {
    newsArray.removeAll()
    queryNews()
    
    if refreshControl.isRefreshing {
//        let formatter = DateFormatter()
//        let date = Date()
//        formatter.dateFormat = "MMM d, h:mm a"
//        let title = "Last update: \(formatter.string(from: date))"
//        let attrsDictionary = NSDictionary(object: UIColor.gray, forKey: NSForegroundColorAttributeName as NSCopying)
//        let attributedTitle = NSAttributedString(string: title, attributes: attrsDictionary as? [String : AnyObject]);
//        refreshControl.attributedTitle = attributedTitle
        refreshControl.endRefreshing()
    }
}
  
 
    
// News
func queryNews() {
    showHUD()
    
    let query = PFQuery(className: NEWS_CLASS_NAME)
    query.order(byDescending: "updatedAt")
    query.findObjectsInBackground { (objects, error)-> Void in
        if error == nil {
            self.newsArray = objects!
            self.newsTableView.reloadData()
            self.hideHUD()
        } else {
            self.simpleAlert("\(error!.localizedDescription)")
            self.hideHUD()
    }}

}
 
    
    
func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}
    
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return newsArray.count
}
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let identifier = indexPath.row == 0 ? "NewsCellHeadLine" : "NewsCell"
    
    let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! NewsCell
    
    var newsClass = PFObject(className: NEWS_CLASS_NAME)
    if  indexPath.row < newsArray.count {
        newsClass = newsArray[indexPath.row]
        
        cell.titleLabel.text = "\(newsClass[NEWS_TITLE]!)"
        let date = newsClass.createdAt
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd MMM, yyyy"
        cell.dateLabel.text = dateFormat.string(from: date!)
        
        if let imageFile = newsClass[NEWS_IMAGE] as? PFFile {
//        imageFile?.getDataInBackground(block: { (imageData, error) -> Void in
//            if error == nil {
//                if let imageData = imageData {
//                    cell.newsImage.image = UIImage(data:imageData)
//        }}})

        cell.newsImage.kf.setImage(with: URL(string: (imageFile.url)!),
                                    placeholder: nil,
                                    options: [.transition(ImageTransition.fade(1))],
                                    progressBlock: nil,
                                    completionHandler: nil)
        }else{
            cell.newsImage.image = nil
        }
    }
    

    
return cell
}
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    return indexPath.row == 0 ?  245 : 130

}
    

// Cell Table
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    var newsClass = PFObject(className: NEWS_CLASS_NAME)
    newsClass = newsArray[(indexPath as NSIndexPath).row]
    
    let aVC = storyboard?.instantiateViewController(withIdentifier: "NewsDetails") as! NewsDetails
    aVC.newsObj = newsClass
    navigationController?.pushViewController(aVC, animated: true)
}
    

    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
