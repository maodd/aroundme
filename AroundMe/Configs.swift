//
//  Configs.swift
//  AroundMe
//
//  Created by Ricgreenfield on 02/06/16.
//  Copyright © 2016 GF. All rights reserved.
//

import Foundation
import UIKit


let APP_NAME = "ParqueMet"

let limitForRecentEventsQuery = 20 // N. of Events to Show - Max 20 Events
let ADMOB_BANNER_UNIT_ID = ""//"ca-app-pub-8256226468487945/6655879299" // Your AdMob ID

let REPORT_EMAIL_ADDRESS = "report@yourdomain.com" //Email when users report inappropriate contents 

let ONESIGNAL_APP_ID = "c1924d23-8008-42e3-b4f7-f8428c5e0e34" // Your OneSignal ID for Push Notifications

let storeCategories = [
    "Todos",
    // Here you can add another categories
    "Accesos",
    "Atractivos",
    "Baños y Camarines",
    "Cumbres",
    "Estacionamientos",
    "Hitos Temáticos",
    "Informaciones",
    "Juegos Infantiles",
    "Miradores",
    "Plazas y Jardines",
    "Punto Limpio",
    "Red de Parque Urbanos",
    "Restaurantes y quioscos",
    "Sendero Ciclista",
    "Sendero Peatonal",
    "Zonas Deportivas",
    "Zonas de Picnic"
]

// Your Parse Keys
let PARSE_APP_KEY = "giBKkfMnXuUZ4Tivut2T2qgSPYBZ5NUI3W9KXBWk"
let PARSE_CLIENT_KEY = "pZtg455076KSddb3NVjTx1iEfpEQE0kqv18FRkNI"

// around me keys
//let PARSE_APP_KEY = "lIWyDk45HreIMtlXx5LadWlDzfhijwN4z2gU8Ka7"
//let PARSE_CLIENT_KEY = "3yccu6S7ACY42OIQ31UfOqH2HzLft2gO1S0ltSHU"

// Parse (Back4app) keys events
//let PARSE_APP_KEY = "5rvayOC9dWYw25mBG1M4wUcH625JzXLngD07geQX"
//let PARSE_CLIENT_KEY = "bXn5WGx9Xdjuk7pNsCpYTK96G0I7N3dr9QvBKyeq"

// Hud View
var hudView = UIView()
var animImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
extension UIViewController {
    func showHUD() {
        return
//        hudView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
//        hudView.backgroundColor = UIColor.white
//        hudView.alpha = 0.9
//        
//        let imagesArr = ["h0", "h1", "h2", "h3", "h4", "h5", "h6", "h7", "h8", "h9"]
//        var images : [UIImage] = []
//        for i in 0..<imagesArr.count {
//            images.append(UIImage(named: imagesArr[i])! )
//        }
//        animImage.animationImages = images
//        animImage.animationDuration = 0.7
//        animImage.center = hudView.center
//        hudView.addSubview(animImage)
//        animImage.startAnimating()
//        
//        view.addSubview(hudView)
    }
    
    func hideHUD() {
//        hudView.removeFromSuperview()
    }
    
    func simpleAlert(_ mess:String) {
        UIAlertView(title: APP_NAME, message: mess, delegate: nil, cancelButtonTitle: "OK").show()
    }
    
}



//Don't edit the code below
var distance = 35.0
var category = ""
let USER_CLASS_NAME = "_User"
let USER_USERNAME = "username"
let USER_PASSWORD = "password"
let USER_EMAIL = "email"
let USER_FULLNAME = "fullname"
let STORES_CLASS_NAME = "Stores"
let STORES_IS_PENDING = "isPending"
let STORES_NAME = "name"
let STORES_CATEGORY = "category"
let STORES_ADDRESS = "address"
let STORES_LOCATION = "location"
let STORES_DESCRIPTION = "description"
let STORES_DESCRIPTION2 = "description2"
let STORES_IMAGE = "image"
let STORES_PHONE = "phone"
let STORES_EMAIL = "email"
let STORES_WEBSITE = "website"
let STORES_REVIEWS = "reviews"
let REVIEWS_CLASS_NAME = "Reviews"
let REVIEWS_TEXT = "text"
let REVIEWS_USER_POINTER = "userPointer"
let REVIEWS_STORE_POINTER = "storePointer"
let REVIEWS_STARS = "stars"
let FAVORITES_CLASS_NAME = "Favorites"
let FAVORITES_INSTALLATION_POINTER = "installationPointer"
let FAVORITES_USER_POINTER = "userPointer"
let FAVORITES_STORE_POINTER = "storePointer"
let NEWS_CLASS_NAME = "News"
let NEWS_TITLE = "title"
let NEWS_TEXT = "text"
let NEWS_IMAGE = "image"

// Events Class
var EVENTS_CLASS_NAME = "Events"
var EVENTS_TITLE = "title"
var EVENTS_DESCRIPTION = "description"

var EVENTS_DESCRIPTION2 = "Description2"
var EVENTS_WEBSITE = "website"
var EVENTS_LOCATION = "location"
var EVENTS_LOCATION_NAME = "location_name"
var EVENTS_ADDRESS = "address"

var EVENTS_START_DATE = "startDate"
var EVENTS_END_DATE = "endDate"
var EVENTS_COST = "cost"
var EVENTS_IMAGE = "image"
var EVENTS_IS_PENDING = "isPending"
var EVENTS_KEYWORDS = "keywords"

 
